
        <div class="row wrapper wrapper-content animated fadeInRight " style="padding-bottom: 10px" >
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-file"></i> Inventory Reports</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Search By Type</label>
                                <select class="form-control filterType">
                                    <option value="All">All</option>
                                    <option value="Stock In">Stock In</option>
                                    <option value="Stock Out">Stock Out</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>Month</label>
                                <select class="form-control filterMonth">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <script>
                                    $('.filterMonth').val('<?php echo date('m') ?>');
                                </script>
                            </div>
                            <div class="col-sm-3">
                                <label>Year</label>
                                <select class="form-control filterYear">
                                    <?php for ($i=date('Y') - 5; $i < date('Y') ; $i++) { 
                                    ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                    }?>
                                    <option selected value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                    <?php for ($i=date('Y') + 1; $i < date('Y') + 5 ; $i++) { 
                                    ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                    }?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>Search</label>
                                <button class="btn btn-primary btn-block btnSearchInventory"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="table-responsive" style="margin-top: 20px">
                            <table class="table table-bordered tableReportInventory">
                                <thead>
                                    <tr>
                                        <th>Item Code</th>
                                        <th>Item</th>
                                        <th>Purchase Price</th>
                                        <th>Selling Price</th>
                                        <th>Quantity</th>
                                        <th>Date</th>
                                        <th>Expiry Date</th>
                                        <th>Estimated Income</th>
                                        <th>Estimated Profit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row wrapper wrapper-content animated fadeInRight " style="padding-bottom: 10px;padding-top: 10px" >
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-file"></i> Rice Tracking Reports</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Filter By Cropping</label>
                                        <select class="form-control filterCroppingRT">
                                            <option value="Annual">Annual</option>
                                            <option value="1st Cropping">1st Cropping</option>
                                            <option value="2nd Cropping">2nd Cropping</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <select class="form-control filterYearRT">
                                            <?php
                                                for ($i=date('Y') - 5; $i < date('Y') ; $i++) { 
                                            ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php
                                                }
                                            ?>
                                            <option selected value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
                                            <?php
                                                for ($i=date('Y') + 1; $i < date('Y') + 10  ; $i++) { 
                                            ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php
                                                }
                                            ?>
                                            
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Search</label>
                                        <button class="btn btn-primary btn-block btnSearchRiceTracking"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover tableRiceTracking">
                                            <thead>
                                                <tr>
                                                    <th>Date Purchased</th>
                                                    <th>Name</th>
                                                    <th>Address</th>
                                                    <th>Sacks</th>
                                                    <th>Total Kilos</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>
                            
                       
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row wrapper wrapper-content animated fadeInRight " style="padding-bottom: 10px;padding-top: 10px" >
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-file"></i> Expiry Items Reports</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Month</label>
                                <select class="form-control filterMonthExpiry">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <script>
                                    $('.filterMonthExpiry').val('<?php echo date('m') ?>');
                                </script>
                            </div>
                            <div class="col-sm-3">
                                <label>Year</label>
                                <select class="form-control filterYearExpiry">
                                    <?php for ($i=date('Y') - 5; $i < date('Y') ; $i++) { 
                                    ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                    }?>
                                    <option selected value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                    <?php for ($i=date('Y') + 1; $i < date('Y') + 5 ; $i++) { 
                                    ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                    }?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>Search</label>
                                <button class="btn btn-primary btn-block btnSearchExpiryItem"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="table-responsive" style="margin-top: 20px">
                            <table class="table table-bordered tableReportExpiryItems">
                                <thead>
                                    <tr>
                                        <th>Item Code</th>
                                        <th>Item</th>
                                        <th>Purchase Price</th>
                                        <th>Selling Price</th>
                                        <th>Quantity</th>
                                        <th>Date</th>
                                        <th>Expiry Date</th>
                                        <th>Estimated Income</th>
                                        <th>Estimated Profit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
                
            </div>
        </div>

<script>
    $('.Reports').addClass('active');
    var type = '<?php echo $userdata['type'] ?>';
    if (type == 'Administrator') {
        var tableReportInventory = $('.tableReportInventory').dataTable({
                pageLength: 10,
                responsive: true,
                "aaSorting": [],
                dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                            { extend: 'copy'},
                            {extend: 'csv'},
                            {extend: 'excel', title: 'Stock In / Out '},
                            {extend: 'pdf', title: 'Stock In / Out '},
                            {extend: 'print',
                             customize: function (win){
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                            .addClass('compact')
                                            .css('font-size', 'inherit');
                            }
                            }
                        ]
            });
        var tableRiceTracking = $('.tableRiceTracking').dataTable({
            'aaSorting':[],
            dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'excel', title: 'Rice Tracking'},
                        {extend: 'pdf', title: 'Rice Tracking'},
                        {extend: 'print',
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]
        });
        var tableReportExpiryItems  = $('.tableReportExpiryItems').dataTable({
                pageLength: 10,
                responsive: true,
                "aaSorting": [],
                dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                            { extend: 'copy'},
                            {extend: 'csv'},
                            {extend: 'excel', title: 'Expiry Items '},
                            {extend: 'pdf', title: 'Expiry Items '},
                            {extend: 'print',
                             customize: function (win){
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                            .addClass('compact')
                                            .css('font-size', 'inherit');
                            }
                            }
                        ]
            });   
    } else {
        var tableReportInventory = $('.tableReportInventory').dataTable({
            pageLength: 10,
            responsive: true,
            "aaSorting": []
        });
        var tableRiceTracking = $('.tableRiceTracking').dataTable({
            'aaSorting':[]
        });
        var tableReportExpiryItems  = $('.tableReportExpiryItems').dataTable({
                pageLength: 10,
                responsive: true,
                "aaSorting": []
            });
    }
    

    $('.btnSearchInventory').on('click',function(){
        var filterType = $('.filterType').val();        
        var filterMonth = $('.filterMonth').val();
        var filterYear = $('.filterYear').val();      
        getInventoryFiltered(filterType,filterMonth,filterYear);
    });

    function getInventoryFiltered(filterType,filterMonth,filterYear)
    {
        $.post("<?php echo base_url('admin/Reports/getInventoryFiltered')?>",
            {filterType,filterMonth,filterYear},
        function(data){
            var data = JSON.parse(data);
            $.each(data,function(key,value){
                var estimated_income = value.quantity * value.selling_price;
                var purchase_price_total = value.purchase_price * value.quantity;
                var estimated_profit = estimated_income - purchase_price_total;
                var newRow = tableReportInventory.fnAddData([value.item_code,value.item,'&#8369;'+ConvertToCurrency(value.purchase_price),'&#8369;'+ConvertToCurrency(value.selling_price),value.quantity,moment(value.date).format('ll'),moment(value.expire_date).format('ll'),'&#8369;'+ConvertToCurrency(estimated_income.toFixed(2)),'&#8369;'+ConvertToCurrency(estimated_profit.toFixed(2))]);
            });
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    $('.btnSearchRiceTracking').on('click',function(){
        var filterCroppingRT = $('.filterCroppingRT').val();        
        var filterYearRT = $('.filterYearRT').val();      
        getTableRiceTracking(filterCroppingRT,filterYearRT);
    });

    // getTableRiceTracking('Annual',moment().format('YYYY'));
    function getTableRiceTracking(filter,year)
    {
        $.post("<?php echo base_url('admin/Rice_tracking/getTableRiceTracking')?>",
            {filter,year},
        function(data){
            var data = JSON.parse(data);
            getTableByData(data);
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    function getTableByData(data)
    {
        tableRiceTracking.fnClearTable();
        $.each(data,function(key,value){
            var td_1 = moment(value.date_purchased).format('ll');
            var td_2 = value.firstname+' '+value.lastname;
            var td_3 = value.address;
            var td_4 = value.sacks;
            var td_5 = value.total_kilos;
            var td_6 = value.total;
            
            var rowIndex = tableRiceTracking.fnAddData([td_1,td_2,td_3,td_4,td_5,'&#8369;'+ConvertToCurrency(td_6)]);
            var row = tableRiceTracking.fnGetNodes(rowIndex);
            $(row).attr('id',value.track_id);
        });
        // callFunction();
    }

    $('.btnSearchExpiryItem').on('click',function(){     
        var filterMonthExpiry = $('.filterMonthExpiry').val();
        var filterYearExpiry = $('.filterYearExpiry').val();      
        getExpiryItemsFiltered(filterMonthExpiry,filterYearExpiry);
    });

    function getExpiryItemsFiltered(filterMonth,filterYear)
    {
        $.post("<?php echo base_url('admin/Reports/getExpiryItemsFiltered')?>",
            {filterMonth,filterYear},
        function(data){
            var data = JSON.parse(data);
            $.each(data,function(key,value){
                var estimated_income = value.quantity * value.selling_price;
                var purchase_price_total = value.purchase_price * value.quantity;
                var estimated_profit = estimated_income - purchase_price_total;
                var newRow = tableReportExpiryItems.fnAddData([value.item_code,value.item,'&#8369;'+ConvertToCurrency(value.purchase_price),'&#8369;'+ConvertToCurrency(value.selling_price),value.quantity,moment(value.date).format('ll'),moment(value.expire_date).format('ll'),'&#8369;'+ConvertToCurrency(estimated_income.toFixed(2)),'&#8369;'+ConvertToCurrency(estimated_profit.toFixed(2))]);
            });
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }
</script>
