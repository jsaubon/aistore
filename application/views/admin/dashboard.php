<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/syntaxhighlighter/scripts/shCore.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/syntaxhighlighter/scripts/shBrushXml.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.dragable.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.trendline.min.js"></script>


<!-- Flot -->
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.symbol.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.time.js"></script>
        <div class="row wrapper wrapper-content animated fadeInRight " style="padding-bottom: 10px;">
            <div class="col-lg-12">
                <div class="ibox float-e-margins" id="ibox1">
                    <div class="ibox-title">
                        <h5><i class="fa fa-money"></i> Sales (Stock Out)</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-white btnChart  btnShowDailyChart">Daily</button>
                                <button type="button" class="btn btn-xs btn-white btnChart active btnShowMonthlyChart">Monthly</button>
                                <button type="button" class="btn btn-xs btn-white btnChart btnShowAnnualChart">Annual</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="flot-chart" >
                                    <div class="flot-chart-content hide" id="flot-line-chart-daily"></div>
                                    <div class="flot-chart-content" id="flot-line-chart-monthly"></div>
                                    <div class="flot-chart-content hide" id="flot-line-chart-annually"></div>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row wrapper wrapper-content animated fadeInRight " style="padding-bottom: 10px;padding-top: 0px">
            <div class="col-lg-12">
                <div class="ibox float-e-margins" id="ibox2">
                    <div class="ibox-title">
                        <h5><i class="fa fa-money"></i> Rice Price Tracking</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="flot-chart" >
                                    <div class="flot-chart-content" id="flot-line-chart-rice-tracking"></div>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
                
            </div>
        </div>



<script>
    $('.Dashboard').addClass('active');
    $('.btnShowDailyChart').on('click',function(){
        hideInventory($(this),$('#flot-line-chart-daily'));
        if ($('#flot-line-chart-daily').html() == '') 
        {
            getInventoryChartDaily();
        }
    });
    $('.btnShowMonthlyChart').on('click',function(){
        hideInventory($(this),$('#flot-line-chart-monthly'));
        // console.log($('#flot-line-chart-monthly').html());
       
        
    });
    $('.btnShowAnnualChart').on('click',function(){
        hideInventory($(this),$('#flot-line-chart-annually'));
        if ($('#flot-line-chart-annually').html() == '') 
        {
            getInventoryChartAnnually();
        }
        
    });

    function hideInventory(button,container)
    {
        $('.btnShowDailyChart').removeClass('active');
        $('.btnShowMonthlyChart').removeClass('active');
        $('.btnShowAnnualChart').removeClass('active');
        $('#flot-line-chart-daily').addClass('hide');
        $('#flot-line-chart-monthly').addClass('hide');
        $('#flot-line-chart-annually').addClass('hide');
        container.removeClass('hide');
        button.addClass('active');
    }
    function daysInMonth(month,year) {
      return new Date(year, month, 0).getDate();
    }
    var inventoryChartDaily = [];
    var inventoryChartDaily_label = [];
    var totalIncome = 0;
    var totalProfit = 0;
    var temp_count_daily = 0;
    var days = 0;
    
    function getInventoryChartDaily()
    {
        var month = moment().format('MM');
        var year = moment().format('YYYY');
        days = daysInMonth(month,year);
        inventoryChartDaily = [];
        for (var i = 0; i < days; i++) {
            // var datetime = new Date(year+'-'+month+'-'+parseInt(i+1)).getTime();
            // console.log(year+'-'+month+'-'+parseInt(i+1));
            var newPoints = [i,0];
            inventoryChartDaily.push(newPoints);
            inventoryChartDaily_label.push([i,moment(year+'-'+month+'-'+parseInt(i+1)).format('ll')]);

        }
        // console.log(inventoryChartDaily);
        temp_count_daily =0 ;

        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

        for (var i = 1; i <= days; i++) {
            totalIncome = 0;
            totalProfit = 0;
            var j = '';
            if(i < 10)
            {
                j = '0'+i;
            }
            else
            {
                j = i;
            }
            var date = year+'-'+month+'-'+j;
            var index = i-1;
            setChartDaily(date,index);
        }
        // console.log(inventoryChartDaily);


    }

    function setChartDaily(date,index)
    {
        $.post("<?php echo base_url('admin/Dashboard/getInventoryChartDaily')?>",
        {date},
        function(data){
            var data = JSON.parse(data);
            if (data.length != 0) 
            {
                $.each(data,function(key,value){
                    // console.log(index);
                    var estimated_income = value.quantity * value.selling_price;
                    var purchase_price_total = value.purchase_price * value.quantity;
                    var estimated_profit = estimated_income - purchase_price_total;
                    inventoryChartDaily[index][1] += estimated_income;
                    // console.log(inventoryChartDaily);
                });
            }
            temp_count_daily++;
            if(temp_count_daily == days)
            {
                var barOptions = {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.0
                                }, {
                                    opacity: 0.0
                                }]
                            }
                        }
                    },
                    xaxis: {
                        // mode: "time",
                        // timeformat: "%Y/%m/%d"
                        // tickSize: [1,'day']
                        ticks: inventoryChartDaily_label
                    },
                    yaxis: {
                        min: 0,
                        // max: 140,
                        tickSize: 5,
                        tickDecimal: 2
                    },
                    colors: ["#1ab394"],
                    grid: {
                        color: "#999999",
                        hoverable: true,
                        clickable: true,
                        tickColor: "#D4D4D4",
                        borderWidth:0
                    },
                    legend: {
                        show: true,
                        position: 'nw'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "Income: %y"
                    }
                };
                totalIncome = 0;
                $.each(inventoryChartDaily,function(key,value){
                    totalIncome += value[1];
                });
                averageIncome = [];
                var avgIncome = totalIncome / days;
                for (var i = 0; i < days; i++) {
                    var month = moment().format('MM');
                    var year = moment().format('YYYY');
                    // var datetime = new Date(year+'-'+month+'-'+parseInt(i+1)).getTime();
                    var newPoints = [i,avgIncome.toFixed(2)];
                    averageIncome.push(newPoints);
                }
                var datasetPoints = [
                                {label: 'Income',data: inventoryChartDaily},
                                {label: 'Average',data: averageIncome,color: 'yellow'}
                            ];
                $.plot($("#flot-line-chart-daily"), datasetPoints, barOptions);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            } 

                
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    var inventoryChartMonthly = [];
    var temp_count_monthly = 0;
    getInventoryChartMonthly();
    function getInventoryChartMonthly()
    {
        var year = moment().format('YYYY');
        inventoryChartMonthly = [];
        for (var i = 0; i < 12; i++) {
            var newPoints = [i,0];
            inventoryChartMonthly.push(newPoints);
        }
        // console.log(inventoryChartMonthly);
        temp_count_monthly =0 ;

        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

        for (var i = 1; i <= 12; i++) {
            totalIncome = 0;
            totalProfit = 0;
            var j = '';
            if(i < 10)
            {
                j = '0'+i;
            }
            else
            {
                j = i;
            }
            var index = i-1;
            setChartMonthly(j,index);
        }
    }
    function setChartMonthly(month,index)
    {
        $.post("<?php echo base_url('admin/Dashboard/getInventoryChartMonthly')?>",
        {month},
        function(data){
            var data = JSON.parse(data);
            // console.log(data);
            if (data.length != 0) 
            {
                $.each(data,function(key,value){
                    // console.log(index);
                    var estimated_income = value.quantity * value.selling_price;
                    var purchase_price_total = value.purchase_price * value.quantity;
                    var estimated_profit = estimated_income - purchase_price_total;
                    inventoryChartMonthly[index][1] += estimated_income;
                });
            }
            // console.log(inventoryChartMonthly);
            temp_count_monthly++;
            // console.log(temp_count_monthly);
            if(temp_count_monthly == 12)
            {
                var barOptions = {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.0
                                }, {
                                    opacity: 0.0
                                }]
                            }
                        }
                    },
                    xaxis: {
                        // mode: "string",
                        // timeformat: "%Y/%m/%d"
                        // tickSize: [1,'day']
                        ticks: [
                        [0,'January'],
                        [1,'February'],
                        [2,'March'],
                        [3,'April'],
                        [4,'May'],
                        [5,'June'],
                        [6,'July'],
                        [7,'August'],
                        [8,'September'],
                        [9,'October'],
                        [10,'November'],
                        [11,'December'],
                        ],
                        tickDecimals:0 
                    },
                    yaxis: {
                        // min: 0,
                        // max: 140,
                        // tickSize: 5,
                        // tickDecimal: 2
                    },
                    colors: ["#1ab394"],
                    grid: {
                        color: "#999999",
                        hoverable: true,
                        clickable: true,
                        tickColor: "#D4D4D4",
                        borderWidth:0
                    },
                    legend: {
                        show: true,
                        position: 'nw'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "Income: %y"
                    }
                };
                totalIncome = 0;
                $.each(inventoryChartMonthly,function(key,value){
                    totalIncome += value[1];
                });
                console.log(inventoryChartMonthly);
                averageIncome = [];
                var avgIncome = totalIncome / 12;
                for (var i = 0; i < 12; i++) {
                    var month = moment().format('MM');
                    var year = moment().format('YYYY');
                    // var datetime = new Date(year+'-'+month+'-'+parseInt(i+1)).getTime();
                    var newPoints = [i,avgIncome.toFixed(2)];
                    averageIncome.push(newPoints);
                }
                var datasetPoints = [
                                {label: 'Income',data: inventoryChartMonthly},
                                {label: 'Average',data: averageIncome,color: 'yellow'}
                            ];
                $.plot($("#flot-line-chart-monthly"), datasetPoints, barOptions);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            } 

                
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    var inventoryChartAnnually = [];
    var inventoryChartAnnually_label = [];
    var temp_count_annually = 0;
    // getInventoryChartMonthly();
    function getInventoryChartAnnually()
    {
        var year = moment().format('YYYY');
        inventoryChartAnnually = [];
        var temp_count = 0;
        for (var i = year; i < parseInt(year) +5; i++) {
            var newPoints = [temp_count,0];

            inventoryChartAnnually.push(newPoints);
            inventoryChartAnnually_label.push([temp_count,i]);
            temp_count++;
        }
        console.log(inventoryChartAnnually_label);
        temp_count_annually =0 ;

        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

        for (var i = 1; i <= 5; i++) {
            totalIncome = 0;
            totalProfit = 0;
           
            var index = i-1;
            setChartAnnually(index + parseInt(year),index);
            // console.log(index + parseInt(year));
        }
    }
    function setChartAnnually(year,index)
    {
        $.post("<?php echo base_url('admin/Dashboard/getinventoryChartAnnually')?>",
        {year},
        function(data){
            var data = JSON.parse(data);
            // console.log(data);
            if (data.length != 0) 
            {
                $.each(data,function(key,value){
                    // console.log(index);
                    var estimated_income = value.quantity * value.selling_price;
                    var purchase_price_total = value.purchase_price * value.quantity;
                    var estimated_profit = estimated_income - purchase_price_total;
                    inventoryChartAnnually[index][1] += estimated_income;
                });
            }
            // console.log(inventoryChartAnnually);
            temp_count_annually++;
            // console.log(temp_count_annually);
            if(temp_count_annually == 5)
            {
                var barOptions = {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.0
                                }, {
                                    opacity: 0.0
                                }]
                            }
                        }
                    },
                    xaxis: {
                        tickSize: 1,
                        tickDecimals: 0,
                        ticks: inventoryChartAnnually_label
                    },
                    yaxis: {
                        // min: 0,
                        // max: 140,
                        // tickSize: 5,
                        // tickDecimal: 2
                    },
                    colors: ["#1ab394"],
                    grid: {
                        color: "#999999",
                        hoverable: true,
                        clickable: true,
                        tickColor: "#D4D4D4",
                        borderWidth:0
                    },
                    legend: {
                        show: true,
                        position: 'nw'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "Income: %y"
                    }
                };
                totalIncome = 0;
                $.each(inventoryChartAnnually,function(key,value){
                    totalIncome += value[1];
                });
                // console.log(inventoryChartAnnually);
                averageIncome = [];
                var avgIncome = totalIncome / 5;
                for (var i = 0; i < 5; i++) {
                    var month = moment().format('MM');
                    var year = moment().format('YYYY');
                    var datetime = new Date(year+'-'+month+'-'+parseInt(i+1)).getTime();
                    var newPoints = [i,avgIncome.toFixed(2)];
                    averageIncome.push(newPoints);
                }
                var datasetPoints = [
                                {label: 'Income',data: inventoryChartAnnually},
                                // {label: 'Average',data: averageIncome,color: 'yellow'}
                            ];
                $.plot($("#flot-line-chart-annually"), datasetPoints, barOptions);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            } 

                
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }
    getRiceTrackingChart();
    function getRiceTrackingChart()
    {
        $('#ibox2').children('.ibox-content').toggleClass('sk-loading');
        $.post("<?php echo base_url('admin/Dashboard/getRiceTrackingChart')?>",
        function(data){
            var data = JSON.parse(data);
            var riceTrackingChartPurchase = [];
            var riceTrackingChartSelling = [];
            var axis_label = [];
            var temp_count = 0;
            $.each(data,function(key,value){
                var newDataPurchase = [temp_count,value.purchase_price];
                var newDataSelling = [temp_count,value.selling_price];
                var newLabel = [temp_count,moment(value.date_added).format('ll')];
                riceTrackingChartPurchase.push(newDataPurchase);
                riceTrackingChartSelling.push(newDataSelling);
                axis_label.push(newLabel);
                temp_count++;
            });
            // console.log(riceTrackingChartPurchase);
            // console.log(riceTrackingChartSelling);
            var barOptions = {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.0
                                }, {
                                    opacity: 0.0
                                }]
                            }
                        }
                    },
                    xaxis: {
                        // mode: null,
                        // timeformat: "%Y/%m/%d"
                        // tickSize: [1,'day']
                        ticks: axis_label
                    },
                    yaxis: {
                        min: 0,
                        // max: 140,
                        tickSize: 5,
                        tickDecimal: 2
                    },
                    colors: ["#1ab394"],
                    grid: {
                        color: "#999999",
                        hoverable: true,
                        clickable: true,
                        tickColor: "#D4D4D4",
                        borderWidth:0
                    },
                    legend: {
                        show: true,
                        position: 'nw'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "Price: %y"
                    }
                };
                var datasetPoints = [
                                {label: 'Purchase Price',data: riceTrackingChartPurchase},
                                {label: 'Selling Price',data: riceTrackingChartSelling,color: 'yellow'}
                            ];
                $.plot($("#flot-line-chart-rice-tracking"), datasetPoints, barOptions);
                $('#ibox2').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }
</script>
