<style>
    .brighttheme-notice {
        background-color: #21B9BB;
        border: 0 solid #ff0;
        color: #ffffff;
    }
</style>
            <div class="row wrapper wrapper-content animated fadeInRight " >
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><i class="fa fa-users"></i> Users</h5>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btnAddUser" data-toggle="modal" data-target="#modal-users"><i class="fa fa-user-circle"></i> Add User</button>
                                    <script>
                                        $('.btnAddUser').on('click',function(){
                                            // $('#modal-users').toggle('modal');
                                            $('#formUser')[0].reset();
                                        });
                                    </script>
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-users" >
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <!-- <th>Password</th> -->
                                    <th>Name</th>
                                    <th>User Type</th>
                                    <th width="10%">Configure</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $key => $value): ?>
                                        <tr id="<?php echo $value['u_id'] ?>">
                                            <td><?php echo $value['username'] ?></td>
                                            <!-- <td><?php echo $value['password'] ?></td> -->
                                            <td><?php echo $value['name'] ?></td>
                                            <td><?php echo $value['type'] ?></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm btnEdit" id="<?php echo $value['u_id'] ?>" data-toggle="modal" data-target="#modal-users"><i class="fa fa-pencil-square-o"></i></button>
                                                <button class="btn btn-danger btn-sm btnDelete" id="<?php echo $value['u_id'] ?>"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                            
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                                
                                </table>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
                   
<div id="modal-users" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                        <form id="formUser" class="form-horizontal" method="POST" enctype="multipart/form-data" action="users/insertusers">
                            <input type="hidden" name="u_id" id="u_id">
                            <h2 style="margin-top: 0px">User Details</h2>
                            <div class="form-group"><label class="col-lg-4 control-label">Username</label>
                                <div class="col-lg-6">
                                    <input type="text" id="username" name="username" placeholder="Username" class="form-control">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-4 control-label">Password</label>
                                <div class="col-lg-6">
                                    <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                                </div>
                            </div>

                            <div class="form-group"><label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-6">
                                    <input type="text"  id="name" name="name" placeholder="Name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-6">
                                    <select name="type" id="type" class="form-control">
                                        <option value="Administrator">Administrator</option>
                                        <option value="BOC Incharge">BOC Incharge</option>
                                        <option value="Encoder">Encoder</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn dim btn-outline btn-md btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                                    <button class="btn dim btn-outline btn-md btn-danger" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
            <input type="hidden" id="base_url" value="<?php echo base_url()?>">
<script>

    
    $('.Users').addClass('active');
    $('.dataTables-users').dataTable();

    $('.paginate_button').on('click',function(){
        $('.btnEdit').on('click',function(){
            var u_id = $(this).attr('id');
            getUser(u_id);
            $('#u_id').val(u_id);
        });

        $('.btnDelete').on('click',function(){
            var u_id = $(this).attr('id');
            // alert(u_id);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this user information!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                delete_user(u_id);
                $('#'+u_id).remove();
                swal("Deleted!", "User info has been deleted.", "success");
            });

            
        });

    });

    $('.btnEdit').on('click',function(){
        var u_id = $(this).attr('id');
        getUser(u_id);
        $('#u_id').val(u_id);
    });

    $('.btnDelete').on('click',function(){
        var u_id = $(this).attr('id');
        // alert(u_id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this user information!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            delete_user(u_id);
            $('#'+u_id).remove();
            swal("Deleted!", "User info has been deleted.", "success");
        });
    });

    $('.btnAddusers').on('click',function(){
        $('#u_id').val('');
    });


    function getUser(u_id)
    {
        $.ajax({
            url: "<?php echo base_url('admin/users/getUser')?>",
            type: 'POST',
            dataType: 'JSON',
            data: {u_id,u_id},
            success: function(data){
                $.each(data,function(key,value){
                    $.each(value,function(k,v){
                        $('input[name="'+k+'"]').val(v);

                    });
                    // alert(value.type);
                    $('#type').val(value.type);
                });
            }
        });
    }


        $('#btnDelete').on('click',function(){
        
    });




     function delete_user(u_id)
     {
        $.ajax({
            url: "<?php echo base_url('admin/users/delete_user')?>",
            type: 'POST',
            dataType: 'JSON',
            data: {u_id:u_id},
            success: function(data){
                console.log(data);
            },
            error: function(error){
                console.log(error);
            }
        });
     }
</script>
