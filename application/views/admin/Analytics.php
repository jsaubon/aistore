<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/syntaxhighlighter/scripts/shCore.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/syntaxhighlighter/scripts/shBrushXml.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.dragable.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.trendline.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"></script>



<!-- Flot -->
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.symbol.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/flot/jquery.flot.time.js"></script>
<style>
    .jqplot-yaxis-label
    {
        left: -30px !important;
    }

</style>
        <div class="row wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px">
            <div class="col-sm-12">
                <div class="ibox float-e-margins" id="ibox2">
                    <div class="ibox-title">
                        <h2>Inventory Trend Line  <span><a id="anLeftYearInventory" style="cursor: pointer;"><i class="fa fa-arrow-left"></i></a><span id="anaYearInventory"><?php echo date('Y') ?></span><a id="anRightYearInventory"  style="cursor: pointer;"><i class="fa fa-arrow-right"></i></a></span></h2>
                        
                    </div>
                    <div class="ibox-content">
                        
                        <div style="padding-left: 15px">
                            <div class="example-plot" id="chart3"></div>
                        </div>
                          
                    </div>
                </div>
                        
            </div>
        </div>
        
        <script class="code" type="text/javascript">
        $(document).ready(function () {

            var d = new Date();
            var yearnow = d.getFullYear();  



            function getInventoryIncomeCount(year,month)
            {
                var count = 0;
                $.ajax({
                    'async': false,
                    url: "<?php echo base_url('admin/Analytics/countInventoryIncomePerMonth')?>",
                    type: "POST",
                    data: {year:year,month:month},
                    success: function(data){
                        count = data;
                    },
                    error: function(data)
                    {
                    }
                });
                return count;
            }


            $('#anLeftYearInventory').on('click',function(){
                var year = $('#anaYearInventory').html();
                year--;
                $('#anaYear').html(year); 
                getChartInventory(year);
            });

            $('#anRightYearInventory').on('click',function(){
                var year = $('#anaYear').html();
                year++;
                $('#anaYear').html(year); 
                getChartInventory(year);
            });

            getChartInventory(yearnow);
            function getChartInventory(year)
            {
                $('#chart3').empty(); 
                var jan = getInventoryIncomeCount(year,'01');
                var feb = getInventoryIncomeCount(year,'02');
                var mar = getInventoryIncomeCount(year,'03');
                var apr = getInventoryIncomeCount(year,'04');
                var may = getInventoryIncomeCount(year,'05');
                var jun = getInventoryIncomeCount(year,'06');
                var jul = getInventoryIncomeCount(year,'07');
                var aug = getInventoryIncomeCount(year,'08');
                var sep = getInventoryIncomeCount(year,'09');
                var oct = getInventoryIncomeCount(year,'10');
                var nov = getInventoryIncomeCount(year,'11');
                var dec = getInventoryIncomeCount(year,'12');

                $.jqplot.config.enablePlugins = true;
                
                s3 = [['1-Jan-08',parseInt(jan)],['1-Feb-08',parseInt(feb)],['1-Mar-08',parseInt(mar)],['1-Apr-08', parseInt(apr)],['1-May-08',parseInt(may)],['1-Jun-08',parseInt(jun)],['1-Jul-08',parseInt(jul)],['1-Aug-08', parseInt(aug)],['1-Sep-08',parseInt(sep)],['1-Oct-08',parseInt(oct)],['1-Nov-08',parseInt(nov)],['1-Dec-08', parseInt(dec)]];
                // console.log(s3);
                plot3 = $.jqplot('chart3',[s3],{
                   title: '',
                   axes: {
                       xaxis: {
                           renderer: $.jqplot.DateAxisRenderer,
                           numberTicks: 12,
                           tickOptions: {
                               formatString: '%B'
                           },
                           tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                                label:'Month', 
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            }
                       },
                       yaxis: {
                        labelRenderer:$.jqplot.CanvasAxisLabelRenderer,
                                label:'Sales', 
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            }
                       }
                   },
                   highlighter: {
                       sizeAdjust: 10,
                       tooltipLocation: 'n',
                       tooltipAxes: 'y',
                       tooltipFormatString: '<b><i><span style="color:#1ab394 ;">Sales</span></i></b> %d',
                       useAxesFormatters: false
                   },
                      series: [
                          {
                              isDragable: false,
                              color: '#1ab394',
                              trendline: {
                                  color: '#FFFF00',
                                  lineWidth: '4'
                              }
                          }
                      ]
                });
            }

            $(window).resize(function() {
                plot3.replot({ resetAxes: true });
            });

        });
        </script>

        <div class="row wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;padding-top: 0px">
            <div class="col-sm-12">
                <div class="ibox float-e-margins" id="ibox2">
                    <div class="ibox-title">
                        <h2>Per Item Trend Line  <span><a id="anLeftYear" style="cursor: pointer;"><i class="fa fa-arrow-left"></i></a><span id="anaYear"><?php echo date('Y') ?></span><a id="anRightYear"  style="cursor: pointer;"><i class="fa fa-arrow-right"></i></a></span></h2>
                        
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label>Select Item</label>
                                <select class="form-control selectItem chosen-select">
                                    <option value="rice1kg">rice1kg - rice 1kg</option>
                                    <?php foreach ($items as $key => $value): ?>
                                        <option value="<?php echo $value['item_code'] ?>"><?php echo $value['item_code'].' - '.$value['item'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div style="padding-left: 15px">
                            <div class="example-plot" id="chart1"></div>  
                        </div>
                        
                    </div>
                </div>
                        
            </div>
        </div>
        
        <script class="code" type="text/javascript">
        $(document).ready(function () {

            var d = new Date();
            var yearnow = d.getFullYear();  

            $('.selectItem').on('change',function(){
                var item_code = $(this).val();
                var year = $('#anaYear').html();
                getChartPerItem(item_code,year);
            });



            function getIncomeCount(item,year,month)
            {
                var count = 0;
                $.ajax({
                    'async': false,
                    url: "<?php echo base_url('admin/Analytics/countItemIncomePerMonth')?>",
                    type: "POST",
                    data: {item:item,year:year,month:month},
                    success: function(data){
                        count = data;
                    },
                    error: function(data)
                    {
                    }
                });
                return count;
            }


            $('#anLeftYear').on('click',function(){
                var year = $('#anaYear').html();
                var item_code = $('.selectItem').val();
                year--;
                $('#anaYear').html(year); 
                getChartPerItem(item_code,year);
            });

            $('#anRightYear').on('click',function(){
                var year = $('#anaYear').html();
                var item_code = $('.selectItem').val();
                year++;
                $('#anaYear').html(year); 
                getChartPerItem(item_code,year);
            });

            getChartPerItem('rice1kg',yearnow);
            function getChartPerItem(item,year)
            {
                $('#chart1').empty(); 
                var jan = getIncomeCount(item,year,'01');
                var feb = getIncomeCount(item,year,'02');
                var mar = getIncomeCount(item,year,'03');
                var apr = getIncomeCount(item,year,'04');
                var may = getIncomeCount(item,year,'05');
                var jun = getIncomeCount(item,year,'06');
                var jul = getIncomeCount(item,year,'07');
                var aug = getIncomeCount(item,year,'08');
                var sep = getIncomeCount(item,year,'09');
                var oct = getIncomeCount(item,year,'10');
                var nov = getIncomeCount(item,year,'11');
                var dec = getIncomeCount(item,year,'12');
                $.jqplot.config.enablePlugins = true;
                
                s1 = [['1-Jan-08',parseInt(jan)],['1-Feb-08',parseInt(feb)],['1-Mar-08',parseInt(mar)],['1-Apr-08', parseInt(apr)],['1-May-08',parseInt(may)],['1-Jun-08',parseInt(jun)],['1-Jul-08',parseInt(jul)],['1-Aug-08', parseInt(aug)],['1-Sep-08',parseInt(sep)],['1-Oct-08',parseInt(oct)],['1-Nov-08',parseInt(nov)],['1-Dec-08', parseInt(dec)]];
                // console.log(s1);
                plot1 = $.jqplot('chart1',[s1],{
                   title: '',
                   axes: {
                       xaxis: {
                           renderer: $.jqplot.DateAxisRenderer,
                           numberTicks: 12,
                           tickOptions: {
                               formatString: '%B'
                           },
                           tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                                label:'Month', 
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            }
                       },
                       yaxis: {
                        labelRenderer:$.jqplot.CanvasAxisLabelRenderer,
                                label:'Sales', 
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            }
                       }
                   },
                   highlighter: {
                       sizeAdjust: 10,
                       tooltipLocation: 'n',
                       tooltipAxes: 'y',
                       tooltipFormatString: '<b><i><span style="color:#1ab394 ;">Sales</span></i></b> %d',
                       useAxesFormatters: false
                   },
                      series: [
                          {
                              isDragable: false,
                              color: '#1ab394',
                              trendline: {
                                  color: '#FFFF00',
                                  lineWidth: '4'
                              }
                          }
                      ]
                });
            }

            $(window).resize(function() {
                plot1.replot({ resetAxes: true });
            });

        });
        </script>

        <div class="row wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;padding-top: 0px">
            <div class="col-sm-12">
                <div class="ibox float-e-margins" id="ibox2">
                    <div class="ibox-title">
                        <h2>Cropping Sales Trend Line  <span><a id="anLeftYearSalesCropping" style="cursor: pointer;"><i class="fa fa-arrow-left"></i></a><span id="anaYearCroppingSales"><?php echo date('Y') ?></span><a id="anRightYearSalesCropping"  style="cursor: pointer;"><i class="fa fa-arrow-right"></i></a></span></h2>
                        
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label>Select Cropping</label>
                                <select class="form-control selectSalesCroppingFilter">
                                    <option value="1st Cropping">1st Cropping</option>
                                    <option value="2nd Cropping">2nd Cropping</option>
                                </select>
                            </div>
                        </div>
                        <div style="padding-left: 15px">
                            <div class="example-plot" id="chart2"></div> 
                        </div>
                         
                    </div>
                </div>
                        
            </div>
        </div>
        <script class="code" type="text/javascript">
        $(document).ready(function () {

            var d = new Date();
            var yearnow = d.getFullYear();  

            $('.selectSalesCroppingFilter').on('change',function(){
                var filter = $(this).val();
                var year = $('#anaYearCroppingSales').html();
                getChartPerItem(filter,year);
            });



            function getSalesCountPerCropping(filter,year,month)
            {
                var count = 0;
                $.ajax({
                    'async': false,
                    url: "<?php echo base_url('admin/Analytics/getChartCroppingSales')?>",
                    type: "POST",
                    data: {filter:filter,year:year,month:month},
                    success: function(data){
                        count = data;
                    },
                    error: function(xhr)
                    {
                        console.log(xhr.responseText);
                    }
                });
                return count;
            }


            $('#anLeftYearSalesCropping').on('click',function(){
                var year = $('#anaYearCroppingSales').html();
                var filter = $('.selectSalesCroppingFilter').val();
                year--;
                $('#anaYearCroppingSales').html(year); 
                getChartPerItem(filter,year);
            });

            $('#anRightYearSalesCropping').on('click',function(){
                var year = $('#anaYearCroppingSales').html();
                var filter = $('.selectSalesCroppingFilter').val();
                year++;
                $('#anaYearCroppingSales').html(year); 
                getChartPerItem(filter,year);
            });

            getChartPerItem('1st Cropping',yearnow);
            function getChartPerItem(filter,year)
            {
                $('#chart2').empty(); 
                s2 = [];
                var jan = 0;
                var feb = 0;
                var mar = 0;
                var apr = 0;
                var may = 0;
                var jun = 0;
                var jul = 0;
                var aug = 0;
                var sep = 0;
                var oct = 0;
                var nov = 0;
                var dec = 0;
                if (filter == '1st Cropping') {
                    
                
                    nov = getSalesCountPerCropping(filter,year,'11');
                    dec = getSalesCountPerCropping(filter,year,'12');
                    jan = getSalesCountPerCropping(filter,year,'01');
                    feb = getSalesCountPerCropping(filter,year,'02');
                    mar = getSalesCountPerCropping(filter,year,'03');
                    apr = getSalesCountPerCropping(filter,year,'04');
                    s2 = [['November',parseInt(nov)],['December', parseInt(dec)],['January',parseInt(jan)],['February',parseInt(feb)],['March',parseInt(mar)],['April', parseInt(apr)]];
                }
                else
                {
                    
                    may = getSalesCountPerCropping(filter,year,'05');
                    jun = getSalesCountPerCropping(filter,year,'06');
                    jul = getSalesCountPerCropping(filter,year,'07');
                    aug = getSalesCountPerCropping(filter,year,'08');
                    sep = getSalesCountPerCropping(filter,year,'09');
                    oct = getSalesCountPerCropping(filter,year,'10');
                    s2 = [['May',parseInt(may)],['June',parseInt(jun)],['July',parseInt(jul)],['August', parseInt(aug)],['September',parseInt(sep)],['October',parseInt(oct)]];
                }
                
                    
                
                $.jqplot.config.enablePlugins = true;
                
               
                // console.log(s2);
                plot2 = $.jqplot('chart2',[s2],{
                   title: '',
                   axes: {
                       xaxis: {
                           renderer: $.jqplot.CategoryAxisRenderer,
                            tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                                label:'Cropping', 
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            }
                       },
                       yaxis: {
                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            },
                            label:'Sales'
                       }
                   },
                   highlighter: {
                       sizeAdjust: 10,
                       tooltipLocation: 'n',
                       tooltipAxes: 'y',
                       tooltipFormatString: '<b><i><span style="color:#1ab394 ;">Sales</span></i></b> %d',
                       useAxesFormatters: false
                   },
                      series: [
                          {
                              isDragable: false,
                              color: '#1ab394',
                              trendline: {
                                  color: '#FFFF00',
                                  lineWidth: '4'
                              }
                          }
                      ]
                });
            }

            $(window).resize(function() {
                plot2.replot({ resetAxes: true });
            });

        });
        </script>

        <div class="row wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;padding-top: 0px">
            <div class="col-sm-12">
                <div class="ibox float-e-margins" id="ibox2">
                    <div class="ibox-title">
                        <h2>Variety Sacks Trend Line  <span><a id="anLeftYearSalesCroppingVariety" style="cursor: pointer;"><i class="fa fa-arrow-left"></i></a><span id="anaYearCroppingSalesVariety"><?php echo date('Y') ?></span><a id="anRightYearSalesCroppingVariety"  style="cursor: pointer;"><i class="fa fa-arrow-right"></i></a></span></h2>
                        
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label>Select Cropping</label>
                                <select class="form-control selectSalesCroppingFilterVariety">
                                    <option value="1st Cropping">1st Cropping</option>
                                    <option value="2nd Cropping">2nd Cropping</option>
                                </select>
                            </div>
                        </div>
                        <div style="padding-left: 15px;">
                            <div class="example-plot" id="chart4"></div>  
                        </div>
                        
                    </div>
                </div>
                        
            </div>
        </div>

        <script class="code" type="text/javascript">
        $(document).ready(function () {

            var d = new Date();
            var yearnow = d.getFullYear();  

            $('.selectSalesCroppingFilterVariety').on('change',function(){
                var filter = $(this).val();
                var year = $('#anaYearCroppingSalesVariety').html();
                getChartPerVariety(filter,year);
            });



            function getSalesCountPerCroppingVariety(filter,year)
            {
                var data_ = [];
                $.ajax({
                    'async': false,
                    url: "<?php echo base_url('admin/Analytics/getChartVarietyByCropping')?>",
                    type: "POST",
                    data: {filter:filter,year:year},
                    success: function(data){
                        data_ = JSON.parse(data);
                    },
                    error: function(xhr)
                    {
                        console.log(xhr.responseText);
                    }
                });
                return data_;
            }


            $('#anLeftYearSalesCroppingVariety').on('click',function(){
                var year = $('#anaYearCroppingSalesVariety').html();
                var filter = $('.selectSalesCroppingFilterVariety').val();
                year--;
                $('#anaYearCroppingSalesVariety').html(year); 
                getChartPerVariety(filter,year);
            });

            $('#anRightYearSalesCroppingVariety').on('click',function(){
                var year = $('#anaYearCroppingSalesVariety').html();
                var filter = $('.selectSalesCroppingFilterVariety').val();
                year++;
                $('#anaYearCroppingSalesVariety').html(year); 
                getChartPerVariety(filter,year);
            });

            getChartPerVariety('1st Cropping',yearnow);
            function getChartPerVariety(filter,year)
            {
                $('#chart4').empty(); 
                s4 = [];
                var data = getSalesCountPerCroppingVariety(filter,year);
                $.each(data,function(key,value){
                    var newData = [value.variety,parseInt(value.sacks)];
                    s4.push(newData);
                });
                    
                if (data.length == 0) {
                    swal('Chart Error',"No Transaction Found",'error');
                }
                else
                {
                    $.jqplot.config.enablePlugins = true;
                    plot4 = $.jqplot('chart4',[s4],{
                       title: '',
                       axes: {
                           xaxis: {
                               renderer: $.jqplot.CategoryAxisRenderer,
                                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                                    label:'Variety', 
                                labelOptions:{
                                    fontFamily:'Helvetica',
                                    fontSize: '14pt'
                                }
                           },
                           yaxis: {
                            labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            },
                            label:'Sacks'
                           }
                       },
                       highlighter: {
                           sizeAdjust: 10,
                           tooltipLocation: 'n',
                           tooltipAxes: 'y',
                           tooltipFormatString: '<b><i><span style="color:#1ab394 ;">Sacks</span></i></b> %d',
                           useAxesFormatters: false
                        },
                        series: [
                            {
                                
                                  isDragable: false,renderer:$.jqplot.BarRenderer,
                                color: '#1ab394',
                                trendline: {
                                    color: '#FFFF00',
                                    lineWidth: '4'
                                },
                            }
                        ],
                        axesDefaults: {
                            tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                            tickOptions: {
                              angle: -30,
                              fontSize: '10pt'
                            }
                        },
                    });
                }
                    
            }

            $(window).resize(function() {
                plot4.replot({ resetAxes: true });
            });

        });
        </script>

        
        <div class="row wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;padding-top: 0px">
            <div class="col-sm-12">
                <div class="ibox float-e-margins" id="ibox2">
                    <div class="ibox-title">
                        <h2>Per Variety Trend Line  <span><a id="anLeftYearPerVariety" style="cursor: pointer;"><i class="fa fa-arrow-left"></i></a><span id="anaYearPerVariety"><?php echo date('Y') ?></span><a id="anRightYearPerVariety"  style="cursor: pointer;"><i class="fa fa-arrow-right"></i></a></span></h2>
                        
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label>Select Item</label>
                                <select class="form-control selectVariety chosen-select">
                                    <?php foreach ($varieties as $key => $value): ?>
                                        <option value="<?php echo $value['variety'] ?>"><?php echo $value['variety']?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div style="padding-left: 15px">
                            <div class="example-plot" id="chart5" ></div>
                        </div>
                          
                    </div>
                </div>
                        
            </div>
        </div>
        
        <script class="code" type="text/javascript">
        $(document).ready(function () {

            var d = new Date();
            var yearnow = d.getFullYear();  

            $('.selectVariety').on('change',function(){
                var variety = $(this).val();
                var year = $('#anaYearPerVariety').html();
                getChartPerVariety(variety,year);
            });



            function getSackCountPerVariety(variety,year,month)
            {
                var count = 0;
                $.ajax({
                    'async': false,
                    url: "<?php echo base_url('admin/Analytics/countSackPerVariety')?>",
                    type: "POST",
                    data: {variety:variety,year:year,month:month},
                    success: function(data){
                        count = data;
                    },
                    error: function(xhr)
                    {
                        console.log(xhr.responseText);
                    }
                });
                return count;
            }


            $('#anLeftYearPerVariety').on('click',function(){
                var year = $('#anaYearPerVariety').html();
                var variety = $('.selectVariety').val();
                year--;
                $('#anaYearPerVariety').html(year); 
                getChartPerVariety(variety,year);
            });

            $('#anRightYearPerVariety').on('click',function(){
                var year = $('#anaYearPerVariety').html();
                var variety = $('.selectVariety').val();
                year++;
                $('#anaYearPerVariety').html(year); 
                getChartPerVariety(variety,year);
            });

            getChartPerVariety($('.selectVariety').val(),yearnow);
            function getChartPerVariety(variety,year)
            {
                $('#chart5').empty(); 
                var jan = getSackCountPerVariety(variety,year,'01');
                var feb = getSackCountPerVariety(variety,year,'02');
                var mar = getSackCountPerVariety(variety,year,'03');
                var apr = getSackCountPerVariety(variety,year,'04');
                var may = getSackCountPerVariety(variety,year,'05');
                var jun = getSackCountPerVariety(variety,year,'06');
                var jul = getSackCountPerVariety(variety,year,'07');
                var aug = getSackCountPerVariety(variety,year,'08');
                var sep = getSackCountPerVariety(variety,year,'09');
                var oct = getSackCountPerVariety(variety,year,'10');
                var nov = getSackCountPerVariety(variety,year,'11');
                var dec = getSackCountPerVariety(variety,year,'12');
                $.jqplot.config.enablePlugins = true;
                
                s5 = [['1-Jan-08',parseInt(jan)],['1-Feb-08',parseInt(feb)],['1-Mar-08',parseInt(mar)],['1-Apr-08', parseInt(apr)],['1-May-08',parseInt(may)],['1-Jun-08',parseInt(jun)],['1-Jul-08',parseInt(jul)],['1-Aug-08', parseInt(aug)],['1-Sep-08',parseInt(sep)],['1-Oct-08',parseInt(oct)],['1-Nov-08',parseInt(nov)],['1-Dec-08', parseInt(dec)]];
                // console.log(s5);
                plot5 = $.jqplot('chart5',[s5],{
                   title: '',
                   axes: {
                       xaxis: {
                           renderer: $.jqplot.DateAxisRenderer,
                           numberTicks: 12,
                           tickOptions: {
                               formatString: '%B'
                           },
                            tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                                label:'Month', 
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            }
                       },
                       yaxis: {
                            labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                            labelOptions:{
                                fontFamily:'Helvetica',
                                fontSize: '14pt'
                            },
                            label:'Sacks'
                       }
                   },
                   highlighter: {
                       sizeAdjust: 10,
                       tooltipLocation: 'n',
                       tooltipAxes: 'y',
                       tooltipFormatString: '<b><i><span style="color:#1ab394 ;">Sacks</span></i></b> %d',
                       useAxesFormatters: false
                   },
                      series: [
                          {
                              isDragable: false,
                              color: '#1ab394',
                              trendline: {
                                  color: '#FFFF00',
                                  lineWidth: '4'
                              }
                          }
                      ]
                });
            }

            $(window).resize(function() {
                plot5.replot({ resetAxes: true });
            });

        });
        </script>

<script>
    $('.Analytics').addClass('active');
    $('.chosen-select').chosen({width: '100%'});
</script>
