<style>
    .no-margin {
        margin: 0px;
    }
</style>

<!-- START MODAL -->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">Modal title</h4>
                <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
            </div>
            <div class="modal-body">
                <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged.</p>
                        <div class="form-group"><label>Sample Input</label> <input type="email" placeholder="Enter your email" class="form-control"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

 <!-- END MODAL -->
        <div class="row wrapper wrapper-content animated fadeInRight " >
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-track"></i> Rice Tracking </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    
                    <div class="ibox-content" style="padding-top: 0px">
                        <small class="btnShowRowNewPrice" style="cursor: pointer;color: #1ab394" ><i>New Price</i></small>
                        <script>
                            $('.btnShowRowNewPrice').on('click',function(){
                                showRowNewRicePrice();                                
                            });
                            function showRowNewRicePrice()
                            {
                                var newRicePrice = $('.newRicePrice');
                                if (newRicePrice.hasClass('hide')) 
                                {
                                    newRicePrice.removeClass('hide');
                                }
                                else
                                {
                                    newRicePrice.addClass('hide');
                                }

                            }
                        </script>
                        <div class="row newRicePrice hide">
                            <form action="<?php echo base_url('admin/Rice_tracking/saveItem') ?>" method="POST">
                                <div class="col-sm-12 no-margin">
                                    <!-- <input type="hidden" name="item_id" id="item_id"> -->
                                    <div class="form-group hide">
                                        <label>Item Code</label> 
                                        <input required type="text" placeholder="Item Code" class="form-control" name="item_code" value="rice1kg">
                                    </div>
                                    <div class="form-group hide">
                                        <label>Item Name</label> 
                                        <input required type="text" placeholder="Item Name" class="form-control" name="item" value="rice1kg">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label>Purchase Price</label> 
                                        <input required type="number" step="0.01" placeholder="Purchase Price" class="form-control" name="purchase_price">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label>Selling Price</label> 
                                        <input required type="number" step="0.01" placeholder="Selling Price" class="form-control" name="selling_price">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label>Save</label> 
                                        <button type="submit" class="btn btn-block btn-primary">Save</button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <!-- <th>Item Code</th> -->
                                                <!-- <th>Item</th> -->
                                                <th>Date Added</th>
                                                <th>Purchase Price</th>
                                                <th>Selling Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($rice_prices as $key => $value): ?>
                                        <tr id="<?php echo $value['item_id'] ?>">
                                            <!-- <td><?php echo $value['item_code'] ?></td> -->
                                            <td><?php echo date('l jS \of F Y h:i:s A',strtotime($value['date_added'])); ?></td>
                                            <td><?php echo $value['purchase_price'] ?></td>
                                            <td><?php echo $value['selling_price'] ?></td>
                                        </tr>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 no-margin">
                                <h3 >Rice Rate : <span class="rice_rate"><?php echo $rice_rate[0]['selling_price'] ?></span><small> /kg</small></h3>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-sm-4">
                                <form method="post" action="<?php echo base_url('admin/Rice_tracking/insertRiceTracking') ?>">
                                    <input type="hidden" name="track_id">
                                    <div class="form-group">
                                        <label>Date Purchased</label>
                                        <input required type="date" name="date_purchased" class="form-control date_purchased" placeholder="Date Purchased" value="<?php echo date('Y-m-d') ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input required type="text" name="firstname" class="form-control" placeholder="First Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input required type="text" name="lastname" class="form-control" placeholder="Last Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input required type="text" name="address" class="form-control" placeholder="Address">
                                    </div>
                                    <div class="form-group">
                                        <label>Variety</label>
                                        <select required name="variety" class="chosen-select" placeholder="Variety">
                                            <option>Select Variety</option>
                                            <?php foreach ($varieties as $key => $value): ?>
                                                <option><?php echo $value['variety'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sacks</label>
                                        <input required type="number" name="sacks" class="form-control" placeholder="Sacks">
                                    </div>
                                    <div class="form-group">
                                        <label>Total Kilos</label>
                                        <input required step="0.01" type="number" name="total_kilos" class="form-control total_kilos" placeholder="Total Kilos">
                                    </div>
                                    <div class="form-group">
                                        <label>Total</label>
                                        <input required type="text" name="total" class="form-control total" placeholder="Total" readonly>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                    </div>
                                </form>
                                <script>
                                    $('.total_kilos').on('keyup',function(){
                                        var total_kilos = parseFloat($(this).val());
                                        var rice_rate = parseFloat($('.rice_rate').html());
                                        console.log(total_kilos+ ' ' +rice_rate);
                                        var total = (total_kilos * rice_rate).toFixed(2);
                                        $('.total').val(total);
                                    });
                                </script>
                            </div>
                            <div class="col-sm-8">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Filter By Cropping</label>
                                        <select class="form-control filterCropping">
                                            <option value="Annual">Annual</option>
                                            <option value="1st Cropping">1st Cropping</option>
                                            <option value="2nd Cropping">2nd Cropping</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <select class="form-control selectYear">
                                            <?php
                                                for ($i=date('Y') - 5; $i < date('Y') ; $i++) { 
                                            ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php
                                                }
                                            ?>
                                            <option selected value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
                                            <?php
                                                for ($i=date('Y') + 1; $i < date('Y') + 10  ; $i++) { 
                                            ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php
                                                }
                                            ?>
                                            
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover tableRiceTracking">
                                            <thead>
                                                <tr>
                                                    <th>Date Purchased</th>
                                                    <th>Name</th>
                                                    <th>Address</th>
                                                    <th>Variety</th>
                                                    <th>Sacks</th>
                                                    <th>Total Kilos</th>
                                                    <th>Total</th>
                                                    <?php if ($userdata['type'] == 'Administrator'): ?>
                                                        <th width="10%">Tools</th>
                                                    <?php endif ?>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <h2 class="pull-right" style="padding-top: 0px">Total :  &#8369;<span id="totalPeso">0</span></h2>
                                </div>
                                    
                            </div>
                       </div>
                    </div>
                </div>
                
            </div>
        </div>


            <input type="hidden" id="base_url" value="<?php echo base_url()?>">
<script>
    $('.Rice_tracking').addClass('active');
    var tableRiceTracking = $('.tableRiceTracking').dataTable({
        'aaSorting':[],
        dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Rice Tracking'},
                    {extend: 'pdf', title: 'Rice Tracking'},
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ],
            "fnDrawCallback": function(){
                callFunctions();
            }
        
    });
    getTableRiceTracking('Annual',moment().format('YYYY'));
    function getTableRiceTracking(filter,year)
    {
        $.post("<?php echo base_url('admin/Rice_tracking/getTableRiceTracking')?>",
            {filter,year},
        function(data){
            var data = JSON.parse(data);
            getTableByData(data);
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    var totalPeso = 0;
    function getTableByData(data)
    {
        totalPeso = 0;
        tableRiceTracking.fnClearTable();
        $.each(data,function(key,value){
            var td_1 = moment(value.date_purchased).format('ll');
            var td_2 = value.firstname+' '+value.lastname;
            var td_3 = value.address;
            var td_4 = value.sacks;
            var td_5 = value.total_kilos;
            var td_6 = value.total;
            var td_8 = value.variety;
            totalPeso += parseInt(td_6);
            var td_7 = '<button class="btn btn-primary btn-sm btnEdit">\
                            <i class="fa fa-pencil"></i>\
                        </button>\
                        <button class="btn btn-danger btn-sm btnDelete">\
                            <i class="fa fa-trash-o"></i>\
                        </button>';
            var type = '<?php echo $userdata['type'] ?>';
            var rowIndex = '';
            if (type == 'Administrator') {
                rowIndex = tableRiceTracking.fnAddData([td_1,td_2,td_3,td_8,td_4,td_5,'&#8369;'+ConvertToCurrency(td_6),td_7]);
            } else {
                rowIndex = tableRiceTracking.fnAddData([td_1,td_2,td_3,td_8,td_4,td_5,'&#8369;'+ConvertToCurrency(td_6)]);
            }
            
            var row = tableRiceTracking.fnGetNodes(rowIndex);
            $(row).attr('id',value.track_id);
        });
        $('#totalPeso').html(totalPeso);
        callFunctions();
    }

    function callFunctions()
    {
        $('.btnDelete').on('click',function(){
            var track_id = $(this).closest('tr').attr('id');
            var tr = $(this).closest('tr');
            // alert(track_id);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Rice Tracking information!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                deleteRiceTrackingByID(track_id,tr);
               
            });
            
        });

        $('.btnEdit').on('click',function(){
            var track_id = $(this).closest('tr').attr('id');
            getRiceTrackingByID(track_id);      
        });
    }

    function deleteRiceTrackingByID(track_id,tr)
    {
        $.post("<?php echo base_url('admin/Rice_tracking/deleteRiceTrackingByID')?>",
            {id:track_id},
        function(data){
            tr.remove();
             swal("Deleted!", "Rice Tracking info has been deleted.", "success");
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    function getRiceTrackingByID(track_id)
    {
        $.post("<?php echo base_url('admin/Rice_tracking/getRiceTrackingByID')?>",
            {id:track_id},
        function(data){
            var data = JSON.parse(data);
            $.each(data,function(key,value){
                $.each(value,function(k,v){
                    $('input[name="'+k+'"]').val(v);
                    $('select[name="'+k+'"]').val(v);
                });
                $('.chosen-select').trigger('chosen:updated');
                var date = moment(value.date_purchased).format('YYYY-MM-DD');
                $('.date_purchased').val(date);
            });
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    tableRiceTracking.on('page.dt',function(){
        callFunctions();
    });

    $('.filterCropping').on('change',function(){
        var filter = $('.filterCropping').val();
        var year = $('.selectYear').val();
        getTableRiceTracking(filter,year);
    });

    $('.selectYear').on('change',function(){
        var filter = $('.filterCropping').val();
        var year = $('.selectYear').val();
        getTableRiceTracking(filter,year);                       
    });

    $('.chosen-select').chosen({width: '100%'});

</script>
