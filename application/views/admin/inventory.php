<style>
    .no-padding 
    {
        padding: 0px !important;
    }
</style>
<!-- START MODAL -->
<div class="modal inmodal" id="modal-items" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cart-plus modal-icon"></i>
                <h4 class="modal-title">Item Details</h4>
                <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
            </div>
            <form action="<?php echo base_url('admin/Inventory/saveItem') ?>" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="item_id" id="item_id">
                    <div class="form-group">
                        <label>Item Code</label> 
                        <input required type="text" placeholder="Item Code" class="form-control" name="item_code">
                    </div>
                    <div class="form-group">
                        <label>Item Name</label> 
                        <input required type="text" placeholder="Item Name" class="form-control" name="item">
                    </div>
                    <div class="form-group">
                        <label>Purchase Price</label> 
                        <input type="number" step="0.01" placeholder="Purchase Price" class="form-control" name="purchase_price">
                    </div>
                    <div class="form-group">
                        <label>Selling Price</label> 
                        <input required type="number" step="0.01" placeholder="Selling Price" class="form-control" name="selling_price">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
            <div class="row wrapper wrapper-content animated fadeInRight " >
                <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><i class="fa fa-file"></i> Items</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="pull-left">
                                        <i class="fa fa-plus" style="color: #1ab394" data-toggle="modal" data-target="#modal-items">Add Item</i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-items" >
                                <thead>
                                <tr>
                                    <th>Item Code</th>
                                    <th>Item</th>
                                    <th>Purchase Price</th>
                                    <th>Selling Price</th>
                                    <th width="20%">Configure</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php foreach ($items as $key => $value): ?>
                                        <tr id="<?php echo $value['item_id'] ?>">
                                            <td><?php echo $value['item_code'] ?></td>
                                            <td><?php echo $value['item'] ?></td>
                                            <td><?php echo $value['purchase_price'] ?></td>
                                            <td><?php echo $value['selling_price'] ?></td>
                                            <td class="text-center">
                                                <button class="btn btn-primary btn-sm btnEdit" id="<?php echo $value['item_id'] ?>" data-toggle="modal" data-target="#modal-items"><i class="fa fa-pencil-square-o"></i></button>
                                                <button class="btn btn-danger btn-sm btnDelete" id="<?php echo $value['item_id'] ?>"><i class="fa fa-trash-o"></i></button>
                                                <button class="btn btn-info btn-sm btnGetStocks" id="<?php echo $value['item_id'] ?>"><i class="fa fa-arrow-right"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach ?> -->
                                </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><i class="fa fa-file"></i> Stocks</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row m-t-xs">
                                            <div class="col-xs-4">
                                                <h5 class="m-b-xs" >Item Code</h5>
                                                <h1 class="no-margins" id="item_code">Item Code</h1>
                                                <!-- <div class="font-bold text-navy">100% <i class="fa fa-bolt"></i></div> -->
                                            </div>
                                            <div class="col-xs-8">
                                                <h5 class="m-b-xs" >Item Name</h5>
                                                <h1 class="no-margins" id="item">Item Name</h1>
                                                <!-- <div class="font-bold text-navy">100% <i class="fa fa-bolt"></i></div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row m-t-xs">
                                            <div class="col-xs-6">
                                                <h5 class="m-b-xs">Purchase Price</span></h5>
                                                <h1 class="no-margins">₱ <span id="purchase_price">0</span></h1>
                                            </div>
                                            <div class="col-xs-6">
                                                <h5 class="m-b-xs">Selling Price</span></h5>
                                                <h1 class="no-margins">₱ <span id="selling_price">0</span></h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="row m-t-xs">
                                            <div class="col-xs-4">
                                                <h5 class="m-b-xs">Total Stock In</h5>
                                                <h1 class="no-margins" id="stock_in">0</h1>
                                                <div class="font-bold text-navy">100% <i class="fa fa-bolt"></i></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <h5 class="m-b-xs">Total Stock Out</h5>
                                                <h1 class="no-margins" id="stock_out">0</h1>
                                                <div class="font-bold text-navy"><span id="stock_out_percent">0%</span> <i class="fa fa-bolt"></i></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <h5 class="m-b-xs">Stocks On-Hand</h5>
                                                <h1 class="no-margins" id="stock_on_hand">0</h1>
                                                <div class="font-bold text-navy"><span id="stock_on_hand_percent">100%</span> <i class="fa fa-bolt"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                <div class="divider"></div>
                                <div class="row" style="border-top: 1px solid #e5e5e5;margin-top: 20px;padding-top: 10px">
                                    <div class="col-xs-12">
                                        <h1 class="" id="item">Stock In / Out Item</h1>
                                    </div>
                                    <iframe name="votar" style="display:none;"></iframe>
                                    <form action="<?php echo base_url('admin/Inventory/saveInventory') ?>" method="POST" target="votar">
                                        <input type="hidden" name="s_id">
                                        <input type="hidden" name="item_id" class="item_id">
                                        <div class="col-xs-4  no-padding">
                                             <div class="form-group">
                                                <label>Type</label> 
                                                <select class="form-control" name="type" id="type">
                                                    <option value="Stock In">Stock In</option>
                                                    <option value="Stock Out">Stock Out</option>
                                                </select>
                                            </div>
                                         </div>
                                         <div class="col-xs-4  no-padding">
                                             <div class="form-group">
                                                <label>Quantity</label> 
                                                <input required type="number" placeholder="Quantity" class="form-control" name="quantity">
                                            </div>
                                         </div>
                                         <div class="col-xs-4  no-padding">
                                             <div class="form-group">
                                                <label>Notes</label> 
                                                <input type="text" placeholder="Notes" class="form-control" name="notes">
                                            </div>
                                         </div>
                                         <div class="col-xs-4  no-padding">
                                             <div class="form-group">
                                                <label>Date</label> 
                                                <input required type="date" placeholder="Date" class="form-control" name="date" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                         </div>
                                         <div class="col-xs-4  no-padding">
                                             <div class="form-group">
                                                <label>Expire Date</label> 
                                                <input type="date" placeholder="Date" class="form-control" name="expire_date">
                                            </div>
                                         </div>
                                         <div class="col-xs-4 no-padding" >
                                             <div class="form-group">
                                                <label style="color: white">as_</label> 
                                                <button class="btn btn-primary btn-block btnSubmitStock" type="submit">Submit</button>
                                            </div>
                                         </div>
                                         </form>
                                </div>
                                <div class="divider"></div>
                                <div class="row" style="border-top: 1px solid #e5e5e5;margin-top: 20px;padding-top: 10px">
                                    <div class="col-xs-12">
                                        <h1 class="" id="item">History</h1>
                                    </div>
                                    <div class="col-sm-12">
                                        <table class="table table-hover table-bordered table-striped tblHistory">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Item Name</th>
                                                    <th>Quantity</th>
                                                    <th>Notes</th>
                                                    <th>Date</th>
                                                    <?php if ($userdata['type'] == 'Administrator'): ?>
                                                        <th width="10%">Configure</th>
                                                    <?php endif ?>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
<script>
    $('.Inventory').addClass('active');
    var dataTables_items = $('.dataTables-items').dataTable({
                                "fnDrawCallback": function(){
                                    callFunctions();
                                }
                            });
    $('.btnAddusers').on('click',function(){
        $('#item_id').val('');
    });
    getTableItems();
    function getTableItems()
    {
        $.post("<?php echo base_url('admin/Inventory/getTableItems')?>",
        function(data){
            var data = JSON.parse(data);
            dataTables_items.fnClearTable();
            $.each(data,function(key,value){
                var td_1 = value.item_code;
                var td_2 = value.item;
                var td_3 = '&#8369;'+ConvertToCurrency(value.purchase_price);
                var td_4 = '&#8369;'+ConvertToCurrency(value.selling_price);
                var btnEdit = '<button class="btn btn-primary btn-sm btnEdit" id="'+value.item_id+'" data-toggle="modal" data-target="#modal-items">\
                                <i class="fa fa-pencil-square-o"></i>\
                            </button>';
                var btnDelete = '<button class="btn btn-danger btn-sm btnDelete" id="'+value.item_id+'">\
                                <i class="fa fa-trash-o"></i>\
                            </button>';
                var btnGetStocks = '<button class="btn btn-info btn-sm btnGetStocks" id="'+value.item_id+'">\
                                <i class="fa fa-arrow-right"></i>\
                            </button>';
                var buttons = '';
                var type = '<?php echo $userdata['type'] ?>';
                if (type == 'Administrator') {
                    buttons = btnEdit+btnDelete+btnGetStocks;
                } else {
                    buttons = btnGetStocks;
                }
                
                var rowIndex = dataTables_items.fnAddData([td_1,td_2,td_3,td_4,buttons]);
                var row = dataTables_items.fnGetNodes(rowIndex);
                $(row).attr('id',value.item_id);
            });
            callFunctions();
        });
    }
    function getItem(item_id)
    {
        $.ajax({
            url: "<?php echo base_url('admin/Inventory/getItem')?>",
            type: 'POST',
            dataType: 'JSON',
            data: {item_id,item_id},
            success: function(data){
                $.each(data,function(key,value){
                    $.each(value,function(k,v){
                        $('input[name="'+k+'"]').val(v);
                    });
                });
            }
        });
    }
    function delete_item(item_id,tr)
    {
        $.ajax({
            url: "<?php echo base_url('admin/Inventory/delete_item')?>",
            type: 'POST',
            data: {item_id:item_id},
            success: function(data){
                console.log(data);
                swal("Deleted!", "Item has been deleted.", "success");
                tr.remove();
            },
            error: function(error){
                console.log(error);
            }
        });
    }
    var tblHistory = $('.tblHistory').dataTable({
            pageLength: 10,
            responsive: true,
            "aaSorting": [],
            dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'excel', title: 'Stock In / Out History'},
                        {extend: 'pdf', title: 'Stock In / Out History'},
                        {extend: 'print',
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]
        });
    function callFunctions()
    {
        $('.btnGetStocks').on('click',function(){
            var item_id = $(this).attr('id');
            // alert(item_id);
            getStocksByItemID(item_id);
        });
        $('.btnEdit').on('click',function(){
            var item_id = $(this).attr('id');
            getItem(item_id);
            $('#item_id').val(item_id);
        });
        $('.btnDelete').on('click',function(){
            var item_id = $(this).attr('id');
            var tr = $(this).closest('tr');
            // alert(item_id);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Item information!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                delete_item(item_id,tr);
                // swal("Deleted!", "User info has been deleted.", "success");
            });
        });
    }

    function getStocksByItemID(item_id)
    {
        $.post("<?php echo base_url('admin/Inventory/getItemByItemID') ?>",{item_id:item_id},function(data){
            var data = JSON.parse(data);
            $.each(data,function(key,value){
                    
                    $('.item_id').val(value.item_id);
                    $('#item_code').html(value.item_code);
                    $('#item').html(value.item);
                    $('#purchase_price').html(value.purchase_price);
                    $('#selling_price').html(value.selling_price);
            });
        });
        $.post("<?php echo base_url('admin/Inventory/getStocksByItemID') ?>",{item_id:item_id},function(data){
            tblHistory.fnClearTable();
            var data = JSON.parse(data);
            var stock_in = 0;
            var stock_out = 0;
            var stock_on_hand = 0;
            $.each(data,function(key,value){
                    var btnDelete = '<button class="btn btn-danger btn-sm btnDeleteStock "><i class="fa fa-trash-o"></i></button>';
                    var type = '<?php echo $userdata['type'] ?>';
                    var rowIndex = '';
                    
                    var date = moment(value.date).format('llll');
                    if (type == 'Administrator') {
                        rowIndex = tblHistory.fnAddData([value.type,value.item,value.quantity,value.notes,date,btnDelete]);
                    } else {
                        rowIndex = tblHistory.fnAddData([value.type,value.item,value.quantity,value.notes,date]);
                    }
                    
                    var row = tblHistory.fnGetNodes(rowIndex);
                    $(row).attr('id',value.s_id);
                    if(value.type == 'Stock In')
                    {
                        stock_in += parseInt(value.quantity);
                        stock_on_hand += parseInt(value.quantity);
                    }
                    else
                    {
                        stock_out += Math.abs(parseInt(value.quantity));
                        stock_on_hand -= Math.abs(parseInt(value.quantity));
                    }
                    $('.item_id').val(value.item_id);
                    // stock_on_hand += parseInt(value.quantity);


                    $('#item_code').html(value.item_code);
                    $('#item').html(value.item);
                    $('#purchase_price').html(value.purchase_price);
                    $('#selling_price').html(value.selling_price);
                    $('#stock_in').html(stock_in);
                    $('#stock_out').html(stock_out);
                    $('#stock_on_hand').html(stock_on_hand);
                    var stock_out_percent = 0;
                    stock_out_percent = (stock_out / stock_in * 100).toFixed(2);
                    var stock_on_hand_percent = 0;
                    stock_on_hand_percent = (stock_on_hand / stock_in * 100).toFixed(2);
                    $('#stock_out_percent').html(stock_out_percent);
                    $('#stock_on_hand_percent').html(stock_on_hand_percent);
            });

            $('.btnDeleteStock').on('click',function(){
                var s_id = $(this).closest('tr').attr('id');
                var tr = $(this).closest('tr');
                 swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this Stock information!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    deleteStockByID(s_id,tr);
                   
                });
            });
            // console.log(stock_in+' '+stock_out+' '+stock_on_hand);
        });
    }

    function deleteStockByID(s_id,tr)
    {
        $.post("<?php echo base_url('admin/Inventory/deleteStockByID')?>",
            {id:s_id},
        function(data){
            tr.remove();
             swal("Deleted!", "Stock info has been deleted.", "success");
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }
    $('.btnSubmitStock').on('click',function(){
        var item_id = $('.item_id').val();
        // alert(item_id);
        setTimeout(function(){
            getStocksByItemID(item_id);
        },2000);
    });
    // dataTables_items.on('page.dt',function(){
    //     callFunctions();
    // });
</script>