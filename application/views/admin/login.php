<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>A &amp; I Store Inventory and Tracking System</title>
    <!-- <link rel="shortcut icon" href="<?php echo base_url('assets/img/cascor.png');?>" /> -->

    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="text-center">
         <h1 class="logo-name" style="font-size: 10vmax;color: black">A &amp; I Store</h1>
    </div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>


            </div>
            <h2>Welcome to A &amp; I</h2>
            <p> Inventory and Tracking System</p>
            <!-- <form class="m-t" role="form" action="loginUser"> -->
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" required="" name="username" id="username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password" id="password">
                </div>
                <button type="button" class="btn btn-primary block full-width m-b dim btn-outline " id="btnSubmit">Login</button>

                
                <p class=" text-center hide" id="Error" style="color:red">Wrong Username or Password <br>Try Again</p>
                <!-- <p href="#" >Try Again!</p> -->
                <!-- <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
            <!-- </form> -->
            <!-- <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p> -->
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>

    <script>
        $('#btnSubmit').on('click',function(){
            var username = $('#username').val();
            var password = $('#password').val();
            $.post("<?php echo base_url('admin/login/loginUser')?>",
                    {username:username,password:password},
                    function(data){
                        // alert(data);
                        if(data == 'Valid')
                        {
                            window.location.href = "<?php echo base_url('admin/Dashboard');?>";
                        }
                        else
                        {
                            $('#Error').removeClass('hide');
                        }
                    }
            );
        });
    </script>

</body>

</html>
