<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>A &amp; I Store Inventory and Tracking System</title>
    <!-- <link rel="shortcut icon" href="<?php echo base_url('assets/img/cascor.png');?>" /> -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url() ?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<!-- 
    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'> -->
<!-- 
    <link href="<?php echo base_url() ?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet"> 
    <link href="<?php echo base_url() ?>assets/pnotify/pnotify.custom.min.css" rel="stylesheet"> 
     <!-- FooTable -->
    <link href="<?php echo base_url() ?>assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url() ?>assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <!-- Mainly scripts -->
    <script src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
     <script src="<?php echo base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url() ?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/moment.js"></script>
    <!-- Clock picker -->
    <script src="<?php echo base_url() ?>assets/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>assets/pnotify/pnotify.custom.min.js"></script>
     <!-- FooTable -->
    <script src="<?php echo base_url() ?>assets/js/plugins/footable/footable.all.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url() ?>assets/js/plugins/chosen/chosen.jquery.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url() ?>assets/js/plugins/toastr/toastr.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url() ?>assets/js/plugins/iCheck/icheck.min.js"></script>
    <!-- Ladda --><!-- 
    <script src="<?php echo base_url() ?>assets/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>
 -->
    <!-- Sweet alert -->
    <script src="<?php echo base_url() ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        function ConvertToCurrency(num)
        {
            var num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            if (!~num.indexOf(".")){
                num = num+'.00';
            }
            return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
    </script>
</head>