<body class="md-skin ">
<?php $userdata = $this->session->userdata['user_data']; ?>
    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element text-center"> <h1 style="color: white">Alfie &amp; Ivy
                            <!-- <img alt="image" class="img-circle img-responsive" src="<?php echo base_url() ?>assets/img/a6.jpg" style="margin: auto;max-height: 100px" /> -->
                             </h1>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">A &amp; I Inventory and Tracking System</strong>
                             </span> <?php echo $userdata['type'] ?><span class="text-muted text-xs block"> <b class="caret"></b></span><?php echo $userdata['name'] ?> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                           
                            <li><a href="login/logout">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        A &amp; I
                    </div>
                </li>
                </li>
                <li class="Dashboard">
                    <a href="<?php echo base_url('admin/Dashboard') ?>"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
                </li>
                <li class="Analytics">
                    <a href="<?php echo base_url('admin/Analytics') ?>"><i class="fa fa-bar-chart"></i> <span class="nav-label">Analytics</span></a>
                </li>
                <li class="Inventory">
                    <a href="<?php echo base_url('admin/Inventory') ?>"><i class="fa fa-file"></i> <span class="nav-label">Inventory</span></a>
                </li>
                <li class="Rice_tracking">
                    <a href="<?php echo base_url('admin/Rice_tracking') ?>"><i class="fa fa-thumb-tack "></i> <span class="nav-label">Rice Tracking</span></a>
                </li>
                <li class="Reports">
                    <a href="<?php echo base_url('admin/Reports') ?>"><i class="fa fa-file "></i> <span class="nav-label">Reports</span></a>
                </li>
                <?php if ($userdata['type'] == 'Administrator'): ?>
                <li class="Users">
                    <a href="<?php echo base_url('admin/Users') ?>"><i class="fa fa-users"></i> <span class="nav-label">Users</span></a>
                </li>    
                <?php endif ?>
                
            </ul>

        </div>
    </nav>