<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {


	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->checklog();
		$data['items'] = $this->getItems();
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/includes/nav-left.php');
		$this->load->view('admin/includes/nav-top.php');
		$this->load->view('admin/Dashboard.php',$data);
		$this->load->view('admin/includes/footer.php');
	}

	function getItems()
	{
		$data = $this->fetchRawData("SELECT * FROM items GROUP BY item_code ORDER BY item_code ASC");
		return $data;
	}

	function getInventoryChartDaily()
	{
		$date = $this->input->post('date');
		// echo $date;
		$data = $this->fetchRawData("SELECT * FROM view_stocks WHERE type='Stock Out' AND DATE(`date`)='$date'");
		echo json_encode($data);
	}
	function getInventoryChartMonthly()
	{
		$month = $this->input->post('month');
		// echo $date;
		$data = $this->fetchRawData("SELECT * FROM view_stocks WHERE type='Stock Out' AND MONTH(`date`)='$month'");
		echo json_encode($data);
	}
	function getinventoryChartAnnually()
	{
		$year = $this->input->post('year');
		// echo $date;
		$data = $this->fetchRawData("SELECT * FROM view_stocks WHERE type='Stock Out' AND YEAR(`date`)='$year'");
		echo json_encode($data);
	}

	function getRiceTrackingChart()
	{
		$data = $this->fetchRawData("SELECT * FROM (SELECT * FROM items WHERE item_code='rice1kg' ORDER BY item_id DESC LIMIT 10)as aaa ORDER BY aaa.item_id ASC");
		echo json_encode($data);
	}

	
	

	
}
