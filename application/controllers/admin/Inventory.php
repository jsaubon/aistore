<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends MY_Controller {


	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->checklog();
		// $data['items'] = $this->getItems();
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/includes/nav-left.php');
		$this->load->view('admin/includes/nav-top.php');
		$this->load->view('admin/inventory.php',$data);
		$this->load->view('admin/includes/footer.php');
	}

	public function getTableItems()
	{
		$data = $this->fetchRawData("SELECT * FROM items WHERE item_code <> 'rice1kg' ORDER BY item_id DESC");
		echo json_encode($data);
	}

	public function saveItem()
	{
		$item_id = $this->input->post('item_id');
		$item_code = $this->input->post('item_code');
		$item = $this->input->post('item');
		$purchase_price = $this->input->post('purchase_price');
		$selling_price = $this->input->post('selling_price');



		// $this->pprint($this->input->post());

		$this->load->model('Model_items');
		$items = new Model_items();

		if($item_id != '')
		{
			$items->item_id = $item_id;
		}

		$items->item_code = $item_code;
		$items->item = $item;
		$items->purchase_price = $purchase_price;
		$items->selling_price = $selling_price;
		$items->date_added = date('Y-m-d H:i:s');
		$items->save();

		redirect(base_url('admin/Inventory'));
	}

	function getItem()
	{
		$item_id = $this->input->post('item_id');
		$data = $this->fetchRawData("SELECT * FROM items WHERE item_id=$item_id");
		echo json_encode($data);
	}

	function delete_item()
	{
		$item_id = $this->input->post('item_id');
		$data = $this->db->query("DELETE FROM items WHERE item_id=$item_id");
		$data1 = $this->db->query("DELETE FROM stocks WHERE item_id=$item_id");
		echo $data;
	}

	function saveInventory()
	{
		$s_id = $this->input->post('s_id');
		$item_id = $this->input->post('item_id');
		$quantity = $this->input->post('quantity');
		$date = $this->input->post('date');
		$type = $this->input->post('type');
		$notes = $this->input->post('notes');
		$expire_date = $this->input->post('expire_date');

		$this->load->model('Model_stocks');
		$stocks = new Model_stocks();

		
		if($s_id != '')
		{
			$stocks->s_id = $s_id;
		}

		$stocks->item_id = $item_id;
		$stocks->quantity = $quantity;
		$stocks->date = $date;
		$stocks->type = $type;
		$stocks->notes = $notes;
		$stocks->expire_date = $expire_date;
		$stocks->save();

		

	}

	function getStocksByItemID()
	{
		$item_id = $this->input->post('item_id');
		$data = $this->fetchRawData("SELECT * FROM view_stocks WHERE item_id=$item_id ORDER BY s_id DESC");
		echo json_encode($data);
	}

	function getItemByItemID()
	{
		$item_id = $this->input->post('item_id');
		$data = $this->fetchRawData("SELECT * FROM items WHERE item_id=$item_id ORDER BY item_id DESC");
		echo json_encode($data);
	}

	function deleteStockByID()
	{
		$id = $this->input->post('id');
		$data = $this->db->query("DELETE FROM stocks WHERE s_id=$id");
		echo 'deleted!';
	}
	

	
}
