<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rice_tracking extends MY_Controller {


	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->checklog();
		$data['rice_rate'] = $this->getRiceRate();
		$data['rice_prices'] = $this->get_rice_prices();
		$data['varieties'] = $this->get_rice_varieties();
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/includes/nav-left.php');
		$this->load->view('admin/includes/nav-top.php');
		$this->load->view('admin/Rice_tracking',$data);
		$this->load->view('admin/includes/footer.php');
	}

	function get_rice_varieties()
	{
		$data = $this->fetchRawData("SELECT * FROM rice_variety");
		return $data;
	}

	function get_rice_prices()
	{
		$data = $this->fetchRawData("SELECT * FROM items WHERE item_code = 'rice1kg' ORDER BY item_id DESC LIMIT 5");
		return $data;
	}

	function getRiceRate()
	{
		$data= $this->fetchRawData("SELECT * FROM items WHERE item_code='rice1kg' ORDER BY item_id DESC Limit 1");
		return $data;
	}

	function insertRiceTracking()
	{
		$track_id = $this->input->post('track_id');
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$address = $this->input->post('address');
		$variety = $this->input->post('variety');
		$sacks = $this->input->post('sacks');
		$total_kilos = $this->input->post('total_kilos');
		$total = $this->input->post('total');
		$date_purchased = $this->input->post('date_purchased');

		$this->load->model('Model_rice_tracking');
		$track = new Model_rice_tracking();
		if ($track_id != '') {
			$track->track_id = $track_id;
		}
		
		$track->firstname = $firstname;
		$track->lastname = $lastname;
		$track->address = $address;
		$track->variety = $variety;
		$track->sacks = $sacks;
		$track->total_kilos = $total_kilos;
		$track->total = $total;
		$track->date_purchased = date('Y-m-d',strtotime($date_purchased));
		$track->save();
		redirect(base_url('admin/Rice_tracking'));

	}

	function getTableRiceTracking()
	{
		$filter = $this->input->post('filter');
		$year = $this->input->post('year');
		if ($filter != 'Annual') {
			if ($filter =='1st Cropping') {
				$data = $this->fetchRawData("SELECT * FROM rice_tracking WHERE MONTH(date_purchased) REGEXP('11|12|1|2|3|4') AND YEAR(date_purchased)=$year ORDER BY track_id DESC");
			}
			else
			{
				$data = $this->fetchRawData("SELECT * FROM rice_tracking WHERE MONTH(date_purchased) REGEXP('5|6|7|8|9|10') AND YEAR(date_purchased)=$year ORDER BY track_id DESC");
			}
			
		}
		else
		{
			$data = $this->fetchRawData("SELECT * FROM rice_tracking ORDER BY track_id DESC");
		}
		
		echo json_encode($data);
	}

	function deleteRiceTrackingByID()
	{
		$id = $this->input->post('id');
		$data = $this->db->query("DELETE FROM rice_tracking WHERE track_id=$id");
		echo 'deleted!';
	}

	function getRiceTrackingByID()
	{
		$id = $this->input->post('id');
		$data = $this->fetchRawData("SELECT * FROM rice_tracking WHERE track_id=$id");
		echo json_encode($data);
	}

	public function saveItem()
	{
		$item_id = $this->input->post('item_id');
		$item_code = $this->input->post('item_code');
		$item = $this->input->post('item');
		$purchase_price = $this->input->post('purchase_price');
		$selling_price = $this->input->post('selling_price');



		// $this->pprint($this->input->post());

		$this->load->model('Model_items');
		$items = new Model_items();

		if($item_id != '')
		{
			$items->item_id = $item_id;
		}

		$items->item_code = $item_code;
		$items->item = $item;
		$items->purchase_price = $purchase_price;
		$items->selling_price = $selling_price;
		$items->date_added = date('Y-m-d H:i:s');
		$items->save();

		redirect(base_url('admin/Rice_tracking'));
	}

	
}
