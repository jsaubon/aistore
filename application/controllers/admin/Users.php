<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {


	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->checklog();
		$data['users'] = $this->getUsers();
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/includes/nav-left.php');
		$this->load->view('admin/includes/nav-top.php');
		$this->load->view('admin/users.php',$data);
		$this->load->view('admin/includes/footer.php');
	}

	public function getUsers()
	{
		$data = $this->fetchRawData("SELECT * FROM users");
		return $data;
	}

	public function insertUsers()
	{
		$u_id = $this->input->post('u_id');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$name = $this->input->post('name');
		$type = $this->input->post('type');



		$this->load->model('Model_users');
		$users = new Model_users();

		if($u_id != '')
		{
			$users->u_id = $u_id;
		}

		$users->username = $username;
		$users->password = $password;
		$users->name = $name;
		$users->type = $type;
		$users->save();
		redirect(base_url('admin/users'));
	}

	function getUser()
	{
		$u_id = $this->input->post('u_id');
		$data = $this->fetchRawData("SELECT * FROM users WHERE u_id=$u_id");
		echo json_encode($data);
	}

	function delete_user()
	{
		$u_id = $this->input->post('u_id');
		$data = $this->db->query("DELETE FROM users WHERE u_id=$u_id");
		echo $data;
	}
	

	
}
