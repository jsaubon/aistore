<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends MY_Controller {


	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->checklog();
		$data['items'] = $this->getItems();
		$data['varieties'] = $this->getVarieties();
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/includes/nav-left.php');
		$this->load->view('admin/includes/nav-top.php');
		$this->load->view('admin/Analytics.php',$data);
		$this->load->view('admin/includes/footer.php');
	}

	function getVarieties()
	{
		$data = $this->fetchRawData("SELECT * FROM rice_tracking GROUP BY variety ORDER BY variety ASC");
		return $data;	
	}

	function getItems()
	{
		$data = $this->fetchRawData("SELECT * FROM items WHERE item_code <> 'rice1kg' GROUP BY item_code ORDER BY item_code ASC");
		return $data;
	}

	function countInventoryIncomePerMonth()
	{
		$year = $this->input->post('year');
		$month = $this->input->post('month');

		$data = $this->fetchRawData("SELECT IF(SUM(selling_price * quantity) IS NOT NULL,SUM(selling_price * quantity),0) as `income` FROM `view_stocks` WHERE  MONTH(date) = $month AND YEAR(date) = $year;");
		
		echo $data[0]['income'];
	}

	function countItemIncomePerMonth()
	{
		$item = $this->input->post('item');
		$year = $this->input->post('year');
		$month = $this->input->post('month');

		if ($item == 'rice1kg') 
		{
			$data = $this->fetchRawData("SELECT IF(SUM(total) IS NOT NULL,SUM(total),0) as `income` FROM `rice_tracking` WHERE MONTH(date_purchased) = $month AND YEAR(date_purchased) = $year;");
		}
		else
		{
			$data = $this->fetchRawData("SELECT IF(SUM(selling_price * quantity) IS NOT NULL,SUM(selling_price * quantity),0) as `income` FROM `view_stocks` WHERE item_code='$item' AND  MONTH(date) = $month AND YEAR(date) = $year;");
		}
		
		echo $data[0]['income'];
	}

	function countItemIncomePerCropping()
	{
		$item = $this->input->post('item');
		$year = $this->input->post('year');
		$month = $this->input->post('month');

		if ($item == 'rice1kg') 
		{
			$data = $this->fetchRawData("SELECT IF(SUM(total) IS NOT NULL,SUM(total),0) as `income` FROM `rice_tracking` WHERE MONTH(date_purchased) = $month AND YEAR(date_purchased) = $year;");
		}
		else
		{
			$data = $this->fetchRawData("SELECT IF(SUM(selling_price * quantity) IS NOT NULL,SUM(selling_price * quantity),0) as `income` FROM `view_stocks` WHERE item_code='$item' AND  MONTH(date) = $month AND YEAR(date) = $year;");
		}
		
		echo $data[0]['income'];
	}


	function getChartCroppingSales()
	{
		$filter = $this->input->post('filter');
		$year = $this->input->post('year');
		$month = $this->input->post('month');
		if ($filter =='1st Cropping') {
			$data = $this->fetchRawData("SELECT IF(SUM(total) IS NOT NULL,SUM(total),0) as `income` FROM rice_tracking WHERE MONTH(date_purchased) = $month AND YEAR(date_purchased)=$year");
		}
		else
		{
			$data = $this->fetchRawData("SELECT IF(SUM(total) IS NOT NULL,SUM(total),0) as `income` FROM rice_tracking WHERE MONTH(date_purchased) = $month AND YEAR(date_purchased)=$year");
		}
		
		echo $data[0]['income'];
	}

	function getChartVarietyByCropping()
	{
		$filter = $this->input->post('filter');
		// $filter = '1st Cropping';
		$year = $this->input->post('year');
		if ($filter =='1st Cropping') {
			$data = $this->fetchRawData("SELECT variety,IF(SUM(sacks) IS NOT NULL,SUM(sacks),0) as `sacks` FROM rice_tracking WHERE MONTH(date_purchased) REGEXP('11|12|1|2|3|4') AND YEAR(date_purchased)=$year GROUP BY variety");
		}
		else
		{
			$data = $this->fetchRawData("SELECT variety,IF(SUM(sacks) IS NOT NULL,SUM(sacks),0) as `sacks` FROM rice_tracking WHERE MONTH(date_purchased) REGEXP('5|6|7|8|9|10') AND YEAR(date_purchased)=$year GROUP BY variety");
		}
		// $this->pprint($data);
		echo json_encode($data);
	}

	function countSackPerVariety()
	{
		$variety = $this->input->post('variety');
		$year = $this->input->post('year');
		$month = $this->input->post('month');

		$data = $this->fetchRawData("SELECT IF(SUM(sacks) IS NOT NULL,SUM(sacks),0) as `sacks` FROM `rice_tracking` WHERE variety='$variety' AND  MONTH(date_purchased) = $month AND YEAR(date_purchased) = $year;");
		
		
		echo $data[0]['sacks'];
	}


	

	
}
