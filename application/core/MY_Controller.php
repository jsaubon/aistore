<?php

/**
* 
*/
class MY_Controller extends CI_Controller
{
  
  public function __construct()
  {
    parent::__construct();
    
  }

  public function pprint($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
  }

  public function fetchRawData($query) {
    $query = $this->db->query($query);
    return $query->result_array();
  }

  function checkLog(){
    $userdata = $this->session->userdata('user_data');
    if(!empty($userdata) && $this->uri->segment(2) == 'login'){
      redirect(base_url('admin/Dashboard'));
    }else if(empty($userdata) && $this->uri->segment(2) != 'login'){
      redirect(base_url('admin/login'));
    }
  }



  

}