$('.records').addClass('active');
    $('#recordsTable').dataTable({
        "aaSorting": []
    });

    var itemsTable = $('#itemsTable').dataTable({
        "aaSorting": [],
        paging: false,
        "bInfo" : false

    });

    $('.btnGetItems').on('click',function () {
        var cm_id = $(this).closest('tr').attr('value');
        // alert(val);
        // $.notiny({ text: 'test', position: "right-top" });
        getItemsByID(cm_id);
    });

var rowItemValues = [];
var bocValues = [];

    function getItemsByID(cm_id)
    {
        rowItemValues = [];
        $.post("<?php echo base_url('records/getItemsByID')?>",
                {cm_id:cm_id},
                function(data){
                    var data = JSON.parse(data);

                    var button = '<button class="btn btn-info btnAddToBOC"><i class="fa fa-plus"></i>Add to BOC</button>';


                    itemsTable.fnClearTable();
                    $.each(data,function(key,value){
                        var rowIndex = itemsTable.fnAddData([value.voyage_no,value.bl_no,value.quantity,value.unit,value.weight,value.volume,value.use_in_formula,value.description,value.rate,value.shipper,value.consignee,value.paymode,value.servmode,button]);
                        var row = itemsTable.fnGetNodes(rowIndex);
                        $(row).attr('id',value.item_id);
                        $(row).attr('value',value.cm_id);
                    });




                    // START OF ADD TO BOC
                    $('.btnAddToBOC').on('click',function(){
                        // alert();
                        var tr = $(this).closest('tr').find('td').each(function(){
                            rowItemValues.push($(this).html());

                        });
                        // console.log(rowItemValues);
                        var use_in_formula = rowItemValues[6];
                        var volMeasurement = '';
                        if(use_in_formula == 'Volume')
                        {
                            volMeasurement = rowItemValues[5];
                        }
                        else
                        {
                            volMeasurement = rowItemValues[4];
                        }

                        var rawRate = rowItemValues[8];
                        var explodedRate = rawRate.split('/');
                        // console.log(explodedRate);
                        var arrastre_rate = explodedRate[0];
                        var stevedoring_rate = explodedRate[1];

                        var arrastreTotalamount = volMeasurement * arrastre_rate;
                        var stevedoringTotalamount = volMeasurement * stevedoring_rate;
                        var totalAmount = arrastreTotalamount+stevedoringTotalamount;
                        var amount = ConvertToCurrency(parseFloat(arrastreTotalamount).toFixed(2)) +'/'+ConvertToCurrency(parseFloat(stevedoringTotalamount).toFixed(2))+'/'+ConvertToCurrency(parseFloat(totalAmount).toFixed(2));

                        var deleteIcon = '<i class="fa fa-trash btnBocRowDelete" style="cursor: pointer"></i>';
                        // bocValues.push(,rowItemValues[0],rowItemValues[7],rowItemValues[1],rowItemValues[2],volMeasurement,rawRate,amount);





                        // console.log(bocValues);
                        var newBOCRow = '<tr style="background:white">\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+rowItemValues[0]+'</td>\
                                            <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px">'+rowItemValues[7]+'</td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+rowItemValues[1]+'</td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+rowItemValues[2]+'</td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+volMeasurement+'</td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+rawRate+'</td>\
                                            <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px;padding-left:10px">Arrastre = '+ConvertToCurrency(parseFloat(arrastreTotalamount).toFixed(2))+'</td>\
                                        </tr>\
                                        <tr style="background:white">\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                            <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                            <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                            <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px">Stevedoring = '+ConvertToCurrency(parseFloat(stevedoringTotalamount).toFixed(2))+'</td>\
                                        </tr>';

                        $('#BOCTBODY').append(newBOCRow);

                        rowItemValues = [];
                        bocValues = [];

                        $('.btnBocRowDelete').on('click',function(){
                            $(this).closest('tr').remove();
                        });



                    });
                    // END OF ADD TO BOC


                    $('#BOCTBODY').on( 'click', 'td', function () {
                        if($(this).children().length > 0 )
                        {

                        }
                        else
                        {
                            // alert();
                            var innerText = $(this).text();
                            var input = '<input type="text" class="form-control inputEdit" value="'+innerText+'">';
                            $(this).empty();
                            $(this).append(input);
                            $('.inputEdit').focus();

                            $('.inputEdit').focusout(function(){
                                var val = $(this).val();
                                $(this).closest('td').html(val);

                            });
                        }
                        
                    } );


                });

        $('#btnAddRowToBOC').on('click',function(){
            var deleteIcon = '<i class="fa fa-trash btnBocRowDelete" style="cursor: pointer"></i>';
            var newBOCRow = '<tr style="background:white">\
                                <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                                <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                                <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                                <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px"></td>\
                            </tr>';
             $('#BOCTBODY').append(newBOCRow);

             $('.btnBocRowDelete').on('click',function(){
                            $(this).closest('tr').remove();
            });
        });


function ConvertToCurrency(num)
{
    return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}


var rowsToCalculate = [[]];

$('#btnCalculate').on('click',function(){
    rowsToCalculate = [];
    var count = 0;

    var bocTotalAmount= 0;
    $('#BOCTBODY > tr').each(function(){
        count++;
        rowsToCalculate[count] = new Array();
        $(this).find('td').each(function(){
                rowsToCalculate[count].push(($(this).html()));
        });
    });
     // console.log(rowsToCalculate);
     // console.log(bocTotalAmount);
    $.each(rowsToCalculate,function(key,value){
         
        $.each(value,function(k,v){
            // console.log(v);
            if(k == 7)
            {

                var rawTotalAmount = v;
                // console.log(rawTotalAmount);
                var explodedTotalAmount = rawTotalAmount.split('=');
                if(explodedTotalAmount.length != 0)
                {
                    var total = explodedTotalAmount[1].replace(',','');
                    bocTotalAmount = parseFloat(bocTotalAmount) + parseFloat(total);
                }
                
            }
        });
        
       
    });

    var TotalAmount = ConvertToCurrency(bocTotalAmount.toFixed(2));

    var LessPPA = ((bocTotalAmount).toFixed(2) * .10).toFixed(2);
    var TotalWithLess = ((bocTotalAmount.toFixed(2)) - LessPPA).toFixed(2);
    var add_VAT  = (TotalWithLess * .12).toFixed(2);
    // alert(parseFloat(TotalWithLess) + parseFloat(add_VAT));
    var GrandTotal = (parseFloat(TotalWithLess) + parseFloat(add_VAT)).toFixed(2);
    // var GrandTotal = ConvertToCurrency();
     
     var deleteIcon = '<i class="fa fa-trash btnBocRowDelete" style="cursor: pointer"></i>';
     // alert(bocTotalAmount);
     var newBOCRow = '<tr style="background:white">\
                        <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px">'+'Total Amount = '+TotalAmount+'</td>\
                    </tr>\
                    <tr style="background:white">\
                        <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px">'+'Less: PPA = '+ConvertToCurrency(LessPPA)+'</td>\
                    </tr>\
                    <tr style="background:white">\
                        <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px">'+' = '+ConvertToCurrency(TotalWithLess)+'</td>\
                    </tr>\
                    <tr style="background:white">\
                        <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px">'+'add: VAT = '+ConvertToCurrency(add_VAT)+'</td>\
                    </tr>\
                    <tr style="background:white">\
                        <td style="text-align: center;border: 1px solid black;font-size:14px">'+deleteIcon+'</td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align:left;padding-left:10px;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: center;border: 1px solid black;font-size:14px"></td>\
                        <td style="text-align: right;padding-right:5px;border: 1px solid black;font-size:14px">'+'&#8369; = '+ConvertToCurrency(GrandTotal)+'</td>\
                    </tr>';
     $('#BOCTBODY').append(newBOCRow);


     $('.btnBocRowDelete').on('click',function(){
                    $(this).closest('tr').remove();
                });

});


        $('#itemsTable').on( 'click', 'tbody td', function () {
            if($(this).children().length > 0 )
            {

            }
            else
            {
                var innerText = $(this).text();
                var input = '<input type="text" class="form-control inputEdit" value="'+innerText+'">';
                $(this).empty();
                $(this).append(input);
                $('.inputEdit').focus();

                $('.inputEdit').focusout(function(){
                    var val = $(this).val();
                    $(this).closest('td').html(val);

                });
            }
            
        } );




    }

var rowValues = [];
    $('.btnSaveItems').on('click',function(){
        $('#itemsTable > tbody  > tr').each(function() {
            rowValues = [];

            var item_id = $(this).attr('id');
            var cm_id = $(this).attr('value');
            rowValues.push(item_id);
            rowValues.push(cm_id);
            $(this).find("td").each(function(){
               rowValues.push($(this).html());
            });
            editItem(rowValues);
        });
        toastr.success("Items Successfully Edited!");
    });

    function editItem(rowValues)
    {
        $.post("<?php echo base_url('records/editItem')?>",
                {item_id:rowValues[0],cm_id:rowValues[1],voyage_no:rowValues[2],bl_no:rowValues[3],quantity:rowValues[4],unit:rowValues[5],weight:rowValues[6],volume:rowValues[7],use_in_formula:rowValues[8],description:rowValues[9],rate:rowValues[10],shipper:rowValues[11],consignee:rowValues[12],paymode:rowValues[13],servmode:rowValues[14]},
                    function(data){
                        // console.log(data);
                        

                    }
        );
    }

   $("#btnPrint").on('click',function () {
        var contents = $("#BOC").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>&nbsp</title>');
        frameDoc.document.write('</head><body>');
        //Append the external CSS file..
        frameDoc.document.write('<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');

        frameDoc.document.write('<link href="assets/css/printcss.css" rel="stylesheet" type="text/css" />');
        //Append the DIV contents.
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    });

